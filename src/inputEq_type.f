!---------------------------------------------------------------------
!#
!# File:  inputEq_type.f.
!#
!# Purpose:  Declare the common derived types for analyzing the
!#           equilibrium
!#                          
!#           
!#
!## $Id: inputEq_type.f 1276 2009-08-05 21:54:17Z srinath $
!---------------------------------------------------------------------
!> This is the input equilibria data structure which represents the
!! data that one would get from an equilibrium data file for example.
!! The surface quantities are common to both equilibria types, but the
!! rest depend on whether it is a direct or inverse type.
!-----------------------------------------------------------------------
!!----------------------------------------------------------------------
!!    subprogram 0. eqInput.
!!    These define 
!!----------------------------------------------------------------------
      MODULE fgInputEqTypes
      USE fg_local
      USE fg_spline
      USE fg_bicube
      IMPLICIT NONE

      TYPE :: direct_bfield_type          !#SIDL_ST !#SIDL_ST_NAME=directBfType#
        REAL(r8) :: psi
        REAL(r8) :: psir
        REAL(r8) :: psiz
        REAL(r8) :: psirz
        REAL(r8) :: psirr
        REAL(r8) :: psizz
        REAL(r8) :: br
        REAL(r8) :: bz
        REAL(r8) :: brr
        REAL(r8) :: brz
        REAL(r8) :: bzr
        REAL(r8) :: bzz
        REAL(r8) :: f
        REAL(r8) :: f1
        REAL(r8) :: p
        REAL(r8) :: p1
        REAL(r8) :: mach
        REAL(r8) :: mach1
        REAL(r8) :: nd
        REAL(r8) :: pe
        REAL(r8) :: zave
        REAL(r8) :: zeff
      END TYPE direct_bfield_type

      TYPE :: inputEq                !#SIDL_ST
        REAL(r8) :: psio             !< Total poloidal flux
        REAL(r8) :: ro               !< Magnetic axis location:
        REAL(r8) :: zo               !<   (ro,zo)

        REAL(r8) :: zz               !< Z required for quasi-neutrality
  
        INTEGER(i4) :: ms_in         !< Number of radial points for sq_in
        INTEGER(i4) :: nsq_in=8      !< # in sq_in type
        TYPE(spline_type) :: sq_in   !< Surface quantities: F,p,q,M_s,nd,p_e

        ! For inverse types
        INTEGER(i4) :: mt_in         !< Number of poloidal arrays
        INTEGER(i4) :: ntwod_in=6    !< # in twod type
        REAL(r8), DIMENSION(:,:), POINTER :: rg_in    !<  R(psi,theta)
        REAL(r8), DIMENSION(:,:), POINTER :: zg_in    !<  Z(psi,theta)

        ! For direct types
        INTEGER(i4) :: mx                 !< Number of R points 
        INTEGER(i4) :: my                 !< Number of Z points 
        REAL(r8) :: rmin_in               !< Minimum R
        REAL(r8) :: rmax_in               !< Maximum R
        REAL(r8) :: zmin_in               !< Minimum Z
        REAL(r8) :: zmax_in               !< Maximum Z
        REAL(r8) :: psiin_boundary        !< Poloidal flux at separatrix
        TYPE(bicube_type) :: psi_in       !< Poloidal flux on R,Z grid
        TYPE(direct_bfield_type) :: bf    !< Type for get_bfield evals

      END TYPE inputEq

      END MODULE fgInputEqTypes
c-----------------------------------------------------------------------
c     subprogram 1. global_dealloc.
c     deallocate the data
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c     declarations.
c-----------------------------------------------------------------------
      SUBROUTINE eqInputDealloc(eqIn)     !#SIDL_SUB !#SIDL_SUB_NAME=eqInDealloc#
      USE fgInputEqTypes
      IMPLICIT NONE
      TYPE(inputEq), INTENT(INOUT) :: eqIn

      IF (ASSOCIATED(eqIn%sq_in%fs))   CALL spline_dealloc(eqIn%sq_in)
      IF (ASSOCIATED(eqIn%rg_in))      DEALLOCATE(eqIn%rg_in)
      IF (ASSOCIATED(eqIn%zg_in))      DEALLOCATE(eqIn%zg_in)
      IF (ASSOCIATED(eqIn%psi_in%fs))  CALL bicube_dealloc(eqIn%psi_in)

      END SUBROUTINE eqInputDealloc
