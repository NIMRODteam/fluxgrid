c-----------------------------------------------------------------------
c     file analyze.f.
c     diagnosis equilibrium
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c     code organization.
c-----------------------------------------------------------------------
c     1. get_global.
c     2. get_rationalSurface.
c     3. get_stability.
c     4. get_pressure_currents.
c     5  get_transport
c-----------------------------------------------------------------------
c     subprogram 1. get_global.
c     computes global equilibrium parameters.
c     What to calculate here and what to calculate on the fly is a matter
c      of some judgement.  Generally if something is used by more than
c      two subroutines in output.f and code.f, then I put it in here.
c     This explains the lambda, delstr, gsrh, thetasf
c-----------------------------------------------------------------------
      SUBROUTINE get_global(gIn,mEq,gEV,gEP) !#SIDL_SUB !#SIDL_SUB_NAME=getGlobal#
      USE fg_local
      USE fg_physdat
      USE fgControlTypes
      USE fgAnalyzeTypes
      USE fgMapEqTypes
      IMPLICIT NONE
      TYPE(mappedEq), INTENT(IN) :: mEq
      TYPE(controlMap), INTENT(IN) :: gIn
      !These are really OUT but the allocs require INOUT
      TYPE(globalEqDiagnose), INTENT(INOUT) :: gEV
      TYPE(globalEqProfiles), INTENT(INOUT) :: gEP
 
      INTEGER(i4) :: ipsi, itheta, mpsi,mtheta
      TYPE(spline_type) :: gs,hs
      REAL(r8) :: dpsi,r,z,bp0,shift
      REAL(r8) :: gss, gst, bsq, jdotb, ldelstr
      REAL(r8) :: coefficient, psio, teEv,ndns,trnstn
      REAL(r8) :: f,fprime,jacfac,mach,msprime,p,pprime
      REAL(r8), DIMENSION(:), ALLOCATABLE :: psifacs
      REAL(r8), DIMENSION(:), ALLOCATABLE :: zeff,zstar,zzi
      REAL(r8), DIMENSION(:), POINTER :: nd,pt,pe,ptprime,peprime
      REAL(r8), PARAMETER :: k_boltzmann=1.3807e-23
      REAL(r8) :: mtot,meovermi
      REAL(r8) :: tfloor, nfloor
      REAL(r8) :: smallnum
      REAL(r8) :: g11,jac
c-----------------------------------------------------------------------
c     Interfaces block
c-----------------------------------------------------------------------
      INTERFACE
        SUBROUTINE fluxav(mEq,quantity,average)
        USE fg_local
        USE fg_physdat
        USE fgAnalyzeTypes
        USE fgMapEqTypes
        TYPE(mappedEq), INTENT(IN) :: mEq
        REAL(r8), DIMENSION(:,:), INTENT(IN) :: quantity
        REAL(r8), DIMENSION(:), INTENT(OUT) :: average
        END SUBROUTINE fluxav
      END INTERFACE
c-----------------------------------------------------------------------
c     Initializations
c-----------------------------------------------------------------------
      smallnum=SQRT(TINY(smallnum))
      mpsi=mEq%mpsi; mtheta=mEq%mtheta
      psio=mEq%psio
      ALLOCATE(psifacs(0:mpsi))
      IF(.NOT. ALLOCATED(gEP%vprime))  
     &   CALL globalEqProfilesAlloc(mpsi,mtheta,gEP)
      gEV%rmean=(mEq%rs1+mEq%rs2)/2.
      gEV%amean=(mEq%rs2-mEq%rs1)/2.
c-----------------------------------------------------------------------
c     preliminary computations.
c-----------------------------------------------------------------------
      mtot=ms(1)+ms(2)/mEq%zz
      meovermi=mEq%zz*ms(1)/ms(2)
      psifacs=mEq%twod%ys
c-----------------------------------------------------------------------
c     fluxgrid is currently a two-fluid oriented code. These follow
c      Callen's notation for convenience
c-----------------------------------------------------------------------
      ALLOCATE(zeff(0:mpsi),zzi(0:mpsi),zstar(0:mpsi))
      zzi=mEq%zz
      zstar=mEq%sq%fs(:,7)  ! If using pfile, ni=ne/zstar
      zeff=mEq%sq%fs(:,8)
c-----------------------------------------------------------------------
c     integrate poloidal magnetic field over each flux surface.
c     calculate other quantities
c-----------------------------------------------------------------------
      CALL spline_alloc(hs,mpsi,7_i4);    hs%xs=psifacs
      CALL spline_alloc(gs,mtheta,5_i4);  gs%xs=mEq%twod%xs
      DO ipsi=0,mpsi
         DO itheta=0,mtheta
            r=mEq%twod%fs(1,itheta,ipsi)
            z=mEq%twod%fs(2,itheta,ipsi)
            jac=mEq%twod%fs(3,itheta,ipsi)*psio         ! psi_normal
            g11=mEq%twod%fs(4,itheta,ipsi)/psio**2      ! psi_normal
            gs%fs(itheta,1)=jac
            gs%fs(itheta,2)=jac*g11/r**2
            gs%fs(itheta,3)=jac*SQRT(g11)/r
            ! Areas
            gs%fs(itheta,4)=jac/(twopi*r)  ! dA=dV/(2pi R)
            gs%fs(itheta,5)=(r**2+z**2)**0.5 * r        ! dA=rho dtheta R dphi
         ENDDO
         CALL spline_fit(gs,"periodic")
         CALL spline_int(gs)
         hs%fs(ipsi,1)=gs%fsi(mtheta,1)*twopi          ! V'=dV/dpsi_normal
         hs%fs(ipsi,2)=mEq%sq%fs(ipsi,2)*hs%fs(ipsi,1) ! PV'
         hs%fs(ipsi,3)=mEq%sq%fs(ipsi,3)               ! q
         hs%fs(ipsi,4)=gs%fsi(mtheta,2)                ! <B_P**2>
         hs%fs(ipsi,5)=gs%fsi(mtheta,3)                ! <B_P>
         hs%fs(ipsi,6)=gs%fsi(mtheta,4)                ! area_xsect
         hs%fs(ipsi,7)=gs%fsi(mtheta,5)                ! area_surf
      ENDDO
      CALL spline_dealloc(gs)
c-----------------------------------------------------------------------
c     integrate flux functions.
c-----------------------------------------------------------------------
      CALL spline_fit(hs,"extrap")
      CALL spline_int(hs)
c-----------------------------------------------------------------------
c     compute global quantities that require integration
c-----------------------------------------------------------------------
      gEP%crnt=psio*hs%fs(:,4)/(mu0*1.e6)
      !
      gEV%q0=mEq%sq%fs(0,3)-mEq%sq%fs1(0,3)*mEq%sq%xs(0)
      gEP%vprime=hs%fs(:,1)
      gEP%torflux=hs%fsi(:,3)*psio          ! Int(q dpsi)
      ! Get constant of integration: First point is not magnetic axis.
      gEP%torflux=gEP%torflux+mEq%sq%xs(0)*gEV%q0*psio
      gEP%vol=hs%fsi(:,2)
      gEP%area_xsct=hs%fs(:,6)
      gEP%area_surf=hs%fs(:,7)
      ! If close enough to edge, go ahead and extrapolate to get more
      ! accurate values
      IF(gIN%psihigh>0.98) THEN
         dpsi=1-psifacs(mpsi)
!         hs%fs(mpsi,:) =hs%fs(mpsi,:) +hs%fs1(mpsi,:)*dpsi
!         hs%fsi(mpsi,:)=hs%fsi(mpsi,:)+hs%fs(mpsi,:) *dpsi
          hs%fsi(mpsi,:)=hs%fsi(mpsi,:)+
     $                       (hs%fs(mpsi,:)+hs%fs1(mpsi,:)*dpsi/2.)*dpsi
         gEV%bt0=(mEq%sq%fs(mpsi,1)+mEq%sq%fs1(mpsi,1)*dpsi/2)/gEV%rmean
      ENDIF
      gEV%torfluxo=hs%fsi(mpsi,3)*psio+mEq%sq%xs(0)*gEV%q0*psio   ! Extrapolated torflux
      gEP%torflux=gEP%torflux/gEV%torfluxo                        ! Normalized toroidal flux
      gEV%crnt=psio*hs%fs(mpsi,4)/(mu0*1.e6)
      gEV%volume=hs%fsi(mpsi,1)
      gEV%area_xsct=hs%fs(mpsi,6)
      gEV%area_surf=hs%fs(mpsi,7)
      gEV%pave=(hs%fsi(mpsi,2)/hs%fsi(mpsi,1))/mu0
      gEV%betat=2.*gEV%pave*mu0/gEV%bt0**2
      gEV%betan=twopi*100.*gEV%amean*gEV%bt0*gEV%betat/gEV%crnt/twopi
      bp0=psio*hs%fs(mpsi,4)/hs%fs(mpsi,5)
      gEV%betap1=2.*mu0*gEV%pave/bp0**2
      gEV%betap2=4.*hs%fsi(mpsi,2)/((1.e6*mu0*gEV%crnt)**2*mEq%ro)
      gEV%betap3=4.*hs%fsi(mpsi,2)/((1.e6*mu0*gEV%crnt)**2*gEV%rmean)
      hs%fsi(mpsi,4)=hs%fsi(mpsi,4)*psio**2        ! Down below, want it normalized
      gEV%li1=hs%fsi(mpsi,4)/hs%fsi(mpsi,1)/bp0**2.*twopi
      gEV%li2=2*hs%fsi(mpsi,4)/((1.e6*mu0*gEV%crnt)**2.*mEq%ro)*twopi
      gEV%li3=2*hs%fsi(mpsi,4)/((1.e6*mu0*gEV%crnt)**2.*gEV%rmean)*twopi
      CALL spline_dealloc(hs)
c-----------------------------------------------------------------------
c     compute global quantities that require integration
c     Note that because the pressures in equilibrium codes are often
c      set to 0, I explicitly set a tfloor to 1 eV (roughly the lowest
c      temperature seen experimentally in electrons.  Note the ionization
c      energy of hydrogen is 13.6 eV) 
c     nfloor set arbitrarily
c-----------------------------------------------------------------------
      nfloor=1.e8
      tfloor=1.*elementary_q                                ! in Joules!
      pt => mEq%sq%fs(:,2)                                    ! Tot  p * mu0
      pe => mEq%sq%fs(:,6)                                    ! Elec p * mu0
      nd => mEq%sq%fs(:,5)                                    ! num dens
      gEP%te = pe/mu0/nd                                      ! mks energy 
      gEP%ti = zstar*(pt-pe)/mu0/nd                             !  units
      gEP%te_ev = gEP%te/elementary_q                         ! eV
      gEP%ti_ev = gEP%ti/elementary_q                         !  units
      ! Extrapolate to center
      gEV%p0 =(mEq%sq%fs(0,2)-mEq%sq%fs1(0,2)*mEq%sq%xs(0))/mu0
      gEV%pe0=(mEq%sq%fs(0,6)-mEq%sq%fs1(0,6)*mEq%sq%xs(0))/mu0
      gEV%nd0=(mEq%sq%fs(0,5)-mEq%sq%fs1(0,5)*mEq%sq%xs(0))
      gEV%ti0=zzi(0)*(gEV%p0-gEV%pe0)/gEV%nd0/k_boltzmann + tfloor
      gEV%te0=gEV%pe0 /gEV%nd0/k_boltzmann + tfloor
c-----------------------------------------------------------------------
c     compute more geometric quantities
c-----------------------------------------------------------------------
      gEV%aratio=gEV%rmean/gEV%amean
      gEV%kappa=(mEq%zst-mEq%zsb)/2./gEV%amean
      gEV%delta1=(gEV%rmean-mEq%rst)/gEV%amean
      gEV%delta2=(gEV%rmean-mEq%rsb)/gEV%amean
c-----------------------------------------------------------------------
c     Get information on singular surfaces.
c-----------------------------------------------------------------------
      gEV%qmin=min(MINVAL(mEq%sq%fs(:,3)),gEV%q0)
      gEV%qmax=MAXVAL(mEq%sq%fs(:,3))
      gEV%qa=mEq%sq%fs(mpsi,3)+mEq%sq%fs1(mpsi,3)*(1-mEq%sq%xs(mpsi))
c-----------------------------------------------------------------------
c     compute resistive MHD scale factors.  See output.f and fluxgrid.out
c-----------------------------------------------------------------------
      gEV%tauafac=mu0*mtot*gEV%rmean**2/gEV%bt0**2
      gEV%taurfac=gEV%amean**2
      gEV%taua=sqrt(gEV%tauafac*gEV%nd0)
      gEV%v_a=gEV%rmean/gEV%taua
c-----------------------------------------------------------------------
c     Calculate velocities and collision time scales
c-----------------------------------------------------------------------
      gEP%vti=SQRT(2.*ABS(gEP%ti)/ms(2))
      gEP%vte=SQRT(2.*ABS(gEP%te)/ms(1))

      !Diamagnetic drift velocities
      ptprime => mEq%sq%fs1(:,2)                                ! Total p
      peprime => mEq%sq%fs1(:,6)                                ! Elec p

      gEP%vdi=ABS((ptprime-peprime)/mu0/gEV%bt0/(nd/zzi)/elementary_q)
      gEP%vde=ABS(peprime/mu0/gEV%bt0/nd/elementary_q)

      ! Temperature-dependent Coulomb logarithm.  See NRL formulary
      ! Using T_e.  
      DO ipsi=0,mpsi
         teEv=gEP%te_ev(ipsi)
         trnstn=0.5*(1.+tanh((teEv-10.)*90.))
         ndns=mEq%sq%fs(ipsi,5)*1.E-6
         gEP%coulomb_log(ipsi)=
     &        trnstn *(24.-LOG((ndns)**0.5/teEv))
     &   +(1.-trnstn)*(23.-LOG((ndns)**0.5/teEv**1.5))
      ENDDO

      ! For the collision frequencies, Callen's UW-CPTC_09-6R (A9) 
      ! seems the best reference for practical use
      coefficient=3.* (4.*pi*eps0)**2 *SQRT(ms(1))/
     &            (4.*SQRT(2*pi)*elementary_q**4)
      gEP%tau_e=coefficient/gEP%coulomb_log* gEP%te**1.5/(nd*zeff)
      ! This is most likely wrong.  Need T_i/T_e's and some zeff's
      coefficient=coefficient*SQRT(ms(2)/ms(1))
      gEP%tau_i=coefficient/gEP%coulomb_log* 
     &               gEP%ti**1.5/(nd/zzi*(1.+2.*zstar))
c-----------------------------------------------------------------------
c     Compute general plasma time and length scales
c-----------------------------------------------------------------------
      gEV%vt0e=SQRT(2.*k_boltzmann*gEV%te0/ms(1))             ! Thermal 
      gEV%vt0i=SQRT(2.*k_boltzmann*gEV%ti0/ms(2))             !  velocities
      gEV%l_pll=gEv%q0*mEq%ro                                  ! Parallel length
      gEV%omega_pe= SQRT(gEV%nd0*qs(1)**2/(eps0*ms(1)))            ! Plasma
      gEV%omega_pi= SQRT(gEV%nd0*(mEq%zz*qs(1))**2/(eps0*ms(2)))     !  frequencies
      gEV%omega_ce=-qs(1)*gEV%bt0/ms(1)                       ! Cyclotron 
      gEV%omega_ci= qs(2)*mEq%zz*gEV%bt0/ms(2)                  !  frequencies
      gEV%omega_star=gEV%p0/(gEV%nd0*elementary_q*gEV%amean**2*gEV%bt0)
      gEV%omega_te= gEV%vt0e/gEV%l_pll                            ! transit (bounce)
      gEV%omega_ti= gEV%vt0i/gEV%l_pll                            !  frequency
      gEV%nu_e = 1./gEP%tau_e(0)                               ! Collision
      gEV%nu_i = 1./gEP%tau_i(0)                               ! frequencies
      gEV%le_debye =gEV%vt0e/gEV%omega_pe                         ! Debye
      gEV%li_debye =gEV%vt0i/gEV%omega_pi                         !  lengths
      gEV%re_larmor=gEV%vt0e/gEV%omega_ce                         ! Larmor 
      gEV%ri_larmor=gEV%vt0i/gEV%omega_ci                         !  radius
      gEV%le_mfp =gEV%vt0e*gEP%tau_e(0)                           ! Mean-free-path
      gEV%li_mfp =gEV%vt0i*gEP%tau_i(0)                           !  length
c-----------------------------------------------------------------------
c     Calculate the transport coefficient profiles (Braginskii)
c	tau_e,tau_i are the electron and ion collision times
c	Using absolute value on temperature to avoid problems with splines
c???  Should I be using nfloor and tfloor here?
c     To convert from the T's given here to eV, divide by elementary_q
c-----------------------------------------------------------------------
      gEP%eta_prp=ms(1)/(gEP%tau_e*nd*elementary_q**2)
      ! Now that I know eta_prp, calculate a characteristic tau_r
      gEV%taur=gEV%taurfac/(gEP%eta_prp(0)/mu0)

      !NOTE: THESE ARE REALLY CHI'S.  UNITS: m^2/s
      !Note: chi_pll_i/chi_pll_e = 3.3 Z^-4 SQRT(m_e/m_i) (T_i/T_e)^2.5 
      gEP%chi_pll=3.16*ABS(gEP%te)*gEP%tau_e/ms(1)           ! Electron 
      gEP%nu_pll=0.96*ABS(gEP%ti)*gEP%tau_i/ms(2)            ! Ion 

      ! Gyro terms
      gEP%chi_cross=5./2.*ABS(gEP%ti)/ms(1)/(2.*gEV%omega_ci)     !  Electron 
      gEP%nu_cross=ABS(gEP%ti)/ms(2)/(2.*gEV%omega_ci)            ! Ion 

      !Note: chi_prp_e/chi_prp_i = 1.65 Z^-2 SQRT(m_e/m_i) (T_i/T_e)^0.5 
      gEP%chi_prp=2.*ABS(gEP%ti)/
     &          (gEP%tau_i*ms(2)*(mEq%zz*qs(1)*gEV%bt0/ms(2))**2)    ! Ion 
      gEP%nu_prp=6./5.*ABS(gEP%ti)/
     &          (gEP%tau_i*ms(2)*(mEq%zz*qs(1)*gEV%bt0/ms(2))**2)    ! Ion 
c-----------------------------------------------------------------------
c     Calculate lambda=J.B/B^2 and Delstar(psi) 
c	J.B = delstar(psi) F/R^2 - F' gss/R^2
c	B^2 = (F^2 + gss)/R^2
c     We use 2 Delstar(psi's):
c	Delstar(psi)=delsquare(psi) - 2/R Grad(R) dot Grad(psi)
c	Delstar(psi)= - r**2 * pprime - f * fprime = RHS of GS Eq. 
c     We calculate both for checking and because nimrod uses one or the
c      other depending on the j_t flag
c-----------------------------------------------------------------------
      DO ipsi=0,mpsi
          f = mEq%sq%fs(ipsi,1)                        ! R B_tor
          fprime = mEq%sq%fs1(ipsi,1)/psio             ! dF/dpsi
          p = mEq%sq%fs(ipsi,2)                        ! mu0 p
          pprime = mEq%sq%fs1(ipsi,2)/psio             ! mu0 pprime
          mach = mEq%sq%fs(ipsi,4)                     ! Mach
          msprime = mEq%sq%fs1(ipsi,4)/psio            ! Mach'
          DO itheta=0,mtheta
              r = mEq%twod%fs(1,itheta,ipsi)
              jacfac = mEq%twod%fs(3,itheta,ipsi)
              gss = mEq%twod%fs(4,itheta,ipsi)
              gst = mEq%twod%fs(5,itheta,ipsi)
              gEP%delstr(itheta,ipsi) = 
     &       mEq%twod%fsy(4,itheta,ipsi)/psio
     &     + mEq%twod%fsx(5,itheta,ipsi)
     &     + gss/jacfac *  mEq%twod%fsy(3,itheta,ipsi)/psio
     &     + gst/jacfac *  mEq%twod%fsx(3,itheta,ipsi)
     &     - 2._r8/r * (gss *mEq%twod%fsy(1,itheta,ipsi)/psio
     &               + gst *mEq%twod%fsx(1,itheta,ipsi) )
              shift=mach**2*((r/mEq%ro)**2-1.)
              gEP%gsrhs(itheta,ipsi) = - f*fprime - r**2*(pprime 
     &             + p*2.*msprime*mach*((r/mEq%ro)**2-1))*EXP(shift)

              ldelstr= gEP%delstr(itheta,ipsi)
              IF (gIn%j_t == "use_gs") ldelstr= gEP%gsrhs(itheta,ipsi)

              jdotb = (ldelstr*f - fprime*gss)/r**2
              bsq = (f**2 + gss)/r**2
              gEP%lambda(itheta,ipsi) = jdotb/bsq
          ENDDO
      ENDDO
      CALL fluxav(mEq,gEP%lambda,gEP%lambdaprof)                  ! <J.B/B^2>
c-----------------------------------------------------------------------
c     terminate routine.
c-----------------------------------------------------------------------
      RETURN
      END SUBROUTINE get_global
c-----------------------------------------------------------------------
c     subprogram 1. get_rationalSurface.
c     Calculate quantities on the rational surfaces
c-----------------------------------------------------------------------
      SUBROUTINE get_rationalSurface(gIn,mEq,qsS)  !#SIDL_SUB !#SIDL_SUB_NAME=get_rational_surface#
      USE fg_local
      USE fg_physdat
      USE fgControlTypes
      USE fgAnalyzeTypes
      USE fgMapEqTypes
      IMPLICIT NONE
      TYPE(controlMap), INTENT(IN) :: gIn
      TYPE(mappedEq), INTENT(IN) :: mEq
      TYPE(singularSurfaces), INTENT(INOUT) :: qsS
      INTEGER(i4) :: m1,m2,idp,nsurf,num_dp,j,numrs, mpsi,mtheta, im
      REAL(r8) :: q0, qmin, qmax, qa, q1, q2, xrs, q1rs
      INTEGER(i4), DIMENSION(:), ALLOCATABLE :: mdp,ndp
      INTEGER(i4), DIMENSION(10) :: irs
      TYPE(spline_type) :: qprof
c-----------------------------------------------------------------------
c     Interface for q_search
c-----------------------------------------------------------------------
      INTERFACE
        SUBROUTINE q_find(xsf,qsf,qrs,nrs,irs,numrs)
        USE fg_local
        USE fg_physdat
        IMPLICIT NONE
        INTEGER(i4), INTENT(IN) :: nrs
        REAL(r8), INTENT(IN) :: qrs
        REAL(r8), DIMENSION(0:), INTENT(IN) :: xsf,qsf
        INTEGER(i4), INTENT(OUT) :: numrs
        INTEGER(i4), DIMENSION(:), INTENT(INOUT) :: irs
        END SUBROUTINE q_find
      END INTERFACE
      INTERFACE
        SUBROUTINE q_refine(qprof,jrs,qrs,xrs,q1rs)
        USE fg_local
        USE fg_physdat
        USE fg_spline
        IMPLICIT NONE
        TYPE(spline_type), INTENT(INOUT) :: qprof
        INTEGER(i4), INTENT(IN) :: jrs
        REAL(r8), INTENT(IN) :: qrs
        REAL(r8), INTENT(OUT) :: xrs,q1rs
        END SUBROUTINE q_refine
      END INTERFACE
c-----------------------------------------------------------------------
c     Get information on singular surfaces.
c-----------------------------------------------------------------------
      mpsi=mEq%mpsi
      CALL spline_alloc(qprof,mpsi,2_i4)
      qprof%xs=mEq%sq%xs
      qprof%fs(:,1)=mEq%sq%fs(:,3)
      CALL spline_fit(qprof,"extrap")
      q0=mEq%sq%fs(0,3)-mEq%sq%fs1(0,3)*mEq%sq%xs(0)
      qmin=MIN(MINVAL(qprof%fs(:,1)),q0)
      qmax=MAXVAL(qprof%fs(:,1))
      qsS%msing=0; qsS%psising=0.; qsS%qsing=0.; qsS%q1sing=0.
      q1=MAX(qmin,gIN%qsmin);   q2=MIN(qmax,gIN%qsmax)
      m1=INT(gIN%nn*q1)+1;  m2=INT(gIN%nn*q2)
      IF (AINT(gIN%nn*q1)==gIN%nn*q1) m1=INT(gIN%nn*q1)
      num_dp=m2-m1+1
      ALLOCATE(mdp(num_dp),ndp(num_dp))
      DO idp=m1,m2
        mdp(idp-m1+1)=idp; ndp(idp-m1+1)=gIN%nn
      ENDDO
c-----------------------------------------------------------------------
c     Store in global variables
c-----------------------------------------------------------------------
      nsurf=0
      DO idp=1,num_dp
        qa=REAL(mdp(idp))/REAL(ndp(idp))
        ! Find indices and number of poloidal harmonics
        CALL q_find(qprof%xs,qprof%fs(:,1),qa,ndp(idp),irs,numrs)
        ! Use a newton method based on splines to find exact location
        DO im=1,numrs
           nsurf=nsurf+1
           qsS%msing(nsurf)   = mdp(idp)
           qsS%qsing(nsurf)   = qa

           ! Use Newton search for finding more exact location
           CALL q_refine(qprof,irs(im),qa,xrs,q1rs)
           qsS%psising(nsurf) = xrs
           qsS%q1sing(nsurf)= q1rs
        ENDDO
      ENDDO
      qsS%nsing=nsurf
c-----------------------------------------------------------------------
c     terminate routine.
c-----------------------------------------------------------------------
      RETURN
      END SUBROUTINE get_rationalSurface
c-----------------------------------------------------------------------
c     subprogram 2. get_stability.
c     Calculate useful stability parameters
c-----------------------------------------------------------------------
      SUBROUTINE get_stability(mEq,sT)      !#SIDL_SUB !#SIDL_SUB_NAME=getStability#
      USE fg_local
      USE fg_physdat
      USE fcirc_mod
      USE fgAnalyzeTypes
      USE fgMapEqTypes
      IMPLICIT NONE

      TYPE(mappedEq), INTENT(IN) :: mEq
      TYPE(stabilityType), INTENT(INOUT) :: sT
 
      TYPE(spline_type) :: es
      INTEGER(i4) :: itheta, ipsi, mtheta, mpsi
      REAL(r8) :: r, psio
      REAL(r8), DIMENSION(:,:), ALLOCATABLE :: bmod, gss, jacinv, h
      REAL(r8), DIMENSION(:), ALLOCATABLE :: dpdpsi, dqdpsi, qsf, f
      REAL(r8), DIMENSION(:), ALLOCATABLE :: dvdpsi, d2vdpsi2, r2bar
      REAL(r8), DIMENSION(:,:), ALLOCATABLE :: quantity
      REAL(r8), DIMENSION(:), ALLOCATABLE :: aveh1, aveh2, aveh3
      REAL(r8), DIMENSION(:), ALLOCATABLE :: aveh4, aveh5, fcirc
c-----------------------------------------------------------------------
c     Interfaces block
c-----------------------------------------------------------------------
      INTERFACE
        SUBROUTINE fluxav(mEq,quantity,average)
        USE fg_local
        USE fg_physdat
        USE fgAnalyzeTypes
        USE fgMapEqTypes
        TYPE(mappedEq), INTENT(IN) :: mEq
        REAL(r8), DIMENSION(:,:), INTENT(IN) :: quantity
        REAL(r8), DIMENSION(:), INTENT(OUT) :: average
        END SUBROUTINE fluxav
      END INTERFACE
c-----------------------------------------------------------------------
      mpsi=mEq%mpsi; mtheta=mEq%mtheta; psio=mEq%psio
c-----------------------------------------------------------------------
c     Allocate stability arrays
c-----------------------------------------------------------------------
      IF(.NOT. ALLOCATED(sT%dideal)) CALL stabilityTypeAlloc(mpsi,sT)
      ALLOCATE(bmod(0:mtheta,0:mpsi), gss(0:mtheta,0:mpsi))
      ALLOCATE(jacinv(0:mtheta,0:mpsi), h(0:mtheta,0:mpsi))
      ALLOCATE(dpdpsi(0:mpsi), dqdpsi(0:mpsi), qsf(0:mpsi), f(0:mpsi))
      ALLOCATE(dvdpsi(0:mpsi), d2vdpsi2(0:mpsi), r2bar(0:mpsi))
      ALLOCATE(quantity(0:mtheta,0:mpsi))
      ALLOCATE(aveh1(0:mpsi), aveh2(0:mpsi), aveh3(0:mpsi))
      ALLOCATE(aveh4(0:mpsi), aveh5(0:mpsi), fcirc(0:mpsi))  
c-----------------------------------------------------------------------
c     Load stuff up into arrays to make code more readable
c-----------------------------------------------------------------------
      f(:) = mEq%sq%fs(:,1)                         ! R B_tor
      dpdpsi(:) = mEq%sq%fs1(:,2)/psio              ! mu0 dp/dpsi
      qsf(:) = mEq%sq%fs(:,3)
      dqdpsi(:) = mEq%sq%fs1(:,3)/psio

      DO ipsi = 0,mpsi
       DO itheta = 0,mtheta
        r = mEq%twod%fs(1,itheta,ipsi)
        gss(itheta,ipsi) = mEq%twod%fs(4,itheta,ipsi)
        bmod(itheta,ipsi) = (f(ipsi)**2 +gss(itheta,ipsi))**0.5/r
        jacinv(itheta,ipsi) = 1._r8/mEq%twod%fs(3,itheta,ipsi)
       ENDDO
      ENDDO
c-----------------------------------------------------------------------
c     Calculate D_I, D_R, H
c     References:
c	Glasser, Greene, Johnson, Phys. Fluids 18 (1975) 875
c		-- Original paper with D_R, H
c	Greene, Comments on Modern Physics Part E, 17 (1997) 
c		-- Nice form of D_I, D_R, and H
c-----------------------------------------------------------------------
      CALL fluxav(mEq,jacinv,aveh1)
      dvdpsi = 4._r8 *pi**2/aveh1
      CALL spline_alloc(es,mpsi,1_i4)
      es%xs(:) = mEq%sq%xs(:)
      es%fs(:,1) = dvdpsi(:)
      CALL spline_fit(es,"extrap")
      d2vdpsi2(:) = es%fs1(:,1)/psio
      CALL spline_dealloc(es)

      quantity = bmod**2/gss
      CALL fluxav(mEq,quantity,aveh1)               !aveh1 = <B^2/gss>
      quantity = 1._r8/gss
      CALL fluxav(mEq,quantity,aveh2)               !aveh2 = <1/gss>
      quantity = 1._r8/(bmod**2*gss)
      CALL fluxav(mEq,quantity,aveh3)               !aveh3 = <1/(B^2 gss)>
      quantity = 1._r8/bmod**2
      CALL fluxav(mEq,quantity,aveh4)               !aveh4 = <1/B^2>
      quantity = bmod**2
      CALL fluxav(mEq,quantity,aveh5)               !aveh5 = <B^2>



      sT%dideal = -1._r8/4._r8 + dpdpsi*dvdpsi/(4*pi**2*dqdpsi)**2 * (
     &              - aveh1 * d2vdpsi2 + 4*pi**2*dqdpsi * f * aveh2
     &          + dpdpsi*dvdpsi * (aveh1*aveh4 
     &          +          f**2 * (aveh1*aveh3 - aveh2**2) )
     &    )

      sT%hfactor = dpdpsi*dvdpsi/(4*pi**2*dqdpsi)*f*(aveh2-aveh1/aveh5)

      sT%dres = sT%dideal + (1._r8/2._r8 - sT%hfactor)**2

c-----------------------------------------------------------------------
c     Calculate D_nc, alpha_s
c     References:
c       Kruger, Hegna, Callen, Phys. Plas. 5 (1998) 455
c		-- Paper discussing D_nc vs. D_R
c       Chris Hegna, Phys. Plas. 6 (1999) 3980
c		-- Correct theory of D_nc and D_R for nonlinear islands
c-----------------------------------------------------------------------
      CALL calculate_fcirc(mEq,fcirc)

      r2bar(:)=f(:) *dvdpsi(:)/(4 * pi**2 * qsf(:))

      sT%dnc(:) = -1.58*(1.-fcirc(:))/(1.58 - 0.58*fcirc(:)) *
     &   dpdpsi(:)/dqdpsi(:)  * qsf(:) * aveh1(:) * r2bar(:)/aveh5(:)

      DO ipsi = 0,mpsi
         IF (sT%dideal(ipsi) .gt. 0 ) THEN
             sT%alphas(ipsi) = 1._r8
         ELSE
             sT%alphas(ipsi) = 1._r8/2._r8 + SQRT(-sT%dideal(ipsi))     !small index
         ENDIF
      ENDDO
      sT%fcirc=fcirc
c-----------------------------------------------------------------------
c     DEALLOCATE
c-----------------------------------------------------------------------
      DEALLOCATE(bmod,gss,jacinv,h,dpdpsi,dqdpsi,qsf,f,aveh5,fcirc)
      DEALLOCATE(dvdpsi,d2vdpsi2,r2bar,quantity,aveh1,aveh2,aveh3,aveh4)
c-----------------------------------------------------------------------
c     terminate routine.
c-----------------------------------------------------------------------
      RETURN
      END SUBROUTINE get_stability
c-----------------------------------------------------------------------
c     subprogram 3. get_neoclassical
c     calculate flux-surface average components of the current including
c     bootstrap, Pfirsch-Schluter, and diamagnetic currents
c     along with other neoclassical parameters
c-----------------------------------------------------------------------
      SUBROUTINE get_neoclassical(TG,gEP,mEq,ts,pC)      !#SIDL_SUB !#SIDL_SUB_NAME=getNeoclassical#
      !USE fg_local
      USE fgControlTypes
      USE fg_physdat
      USE fcirc_mod
      USE fgAnalyzeTypes
      USE fgMapEqTypes
      IMPLICIT NONE
      TYPE(mappedEq), INTENT(IN) :: mEq
      TYPE(globalEqProfiles), INTENT(IN) :: gEP
      TYPE(TransportGeometry), INTENT(IN) :: TG
      TYPE(neoclassical), INTENT(INOUT) :: pC
      TYPE(controlAux), INTENT(IN) :: ts
      REAL(r8) :: psio
      REAL(r8), DIMENSION(:), ALLOCATABLE :: x,eps,coeff
      REAL(r8), DIMENSION(:), ALLOCATABLE :: zeff,zstar,zzi
      REAL(r8), DIMENSION(:), ALLOCATABLE :: ft,ft31eff,ft34eff,xeff,zp1
      REAL(r8), DIMENSION(:), ALLOCATABLE :: ft32ee_eff,ft32ei_eff,sqnui
      REAL(r8), DIMENSION(:), ALLOCATABLE :: alpha0,sqnue,l34,dd,l32
      REAL(r8), DIMENSION(:), POINTER :: nd,f
      REAL(r8), DIMENSION(:), ALLOCATABLE :: dnd,pp,dpp,pe,dpe,ti,dti
      REAL(r8), DIMENSION(:), ALLOCATABLE :: bdcp2,bdct2,btr2,te,dte
      REAL(r8), DIMENSION(:), ALLOCATABLE :: csharp,spfactorHS
      REAL(r8), DIMENSION(:), ALLOCATABLE :: muOVERnu
      REAL(r8), DIMENSION(:), ALLOCATABLE :: nustareS,clogS,eN,xS,fff
      REAL(r8), DIMENSION(:), ALLOCATABLE :: nustariS,clogiS
      REAL(r8), DIMENSION(:), ALLOCATABLE :: ftcs, hfactorcs, betacs
      REAL(r8), DIMENSION(:), ALLOCATABLE :: deltacs, zalpha
      REAL(r8), DIMENSION(:), ALLOCATABLE :: dbananacs, alphacs
      REAL(r8), DIMENSION(:,:,:,:), ALLOCATABLE :: neomat
      INTEGER(i4) :: mpsi,i,j
c-----------------------------------------------------------------------
c     Interfaces block
c-----------------------------------------------------------------------
      INTERFACE
        SUBROUTINE fluxav(mEq,quantity,average)
        USE fg_local
        USE fg_physdat
        USE fgAnalyzeTypes
        USE fgMapEqTypes
        TYPE(mappedEq), INTENT(IN) :: mEq
        REAL(r8), DIMENSION(:,:), INTENT(IN) :: quantity
        REAL(r8), DIMENSION(:), INTENT(OUT) :: average
        END SUBROUTINE fluxav
      END INTERFACE
c-----------------------------------------------------------------------
c     Do initial alloc setup
c-----------------------------------------------------------------------
      psio=mEq%psio; mpsi=mEq%mpsi
      IF(.NOT. ALLOCATED(pC%jdia)) CALL currentAnalysisAlloc(mpsi,pC)
c-----------------------------------------------------------------------
c     calculate bootstrap coefficients l31,l32, ki
c     Parallel neoclassical resistivity
c     References:
c     1.  S.P. Hirshman Phys. Fluids  (1988)
c     2.  O. Sauter et al, Phys. Plasmas 6 (1999) 2834.
c     3.  J.D. Callen UW-CPTC-09-06R an dJ.D. Callen UW-CPTC-11-05
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c     Sometimes we still need to use eps approximation
c-----------------------------------------------------------------------
      ALLOCATE(x(0:mpsi),eps(0:mpsi),coeff(0:mpsi))
      CALL calculate_fcirc(mEq,pC%fcirc)
      eps=TG%r/TG%bigr
      x(:)=(1.-pC%fcirc)/pC%fcirc
c-----------------------------------------------------------------------
c     fluxgrid is currently a two-fluid oriented code
c-----------------------------------------------------------------------
      ALLOCATE(zeff(0:mpsi),zzi(0:mpsi),zstar(0:mpsi))
      zzi=mEq%zz
      zstar=mEq%sq%fs(:,7)
      zeff=mEq%sq%fs(:,8)
c-----------------------------------------------------------------------
c     Note that in Jim's notation, Tau_ee is the reference frequency and
c      is equal to Zeff/nu_e.  See A.9 from UW-CPTC-09-06R
c     Transit frequency and effective collision frequency, nustar
c     For nustar, use first equality of B12 with B10 and the formula
c      right above B10
c-----------------------------------------------------------------------
      ALLOCATE(muOVERnu(0:mpsi))
      ! See Eq. B10 of UW-CPTC 09-6R
      pC%omegatre=gEP%vte/(TG%bigr*mEq%sq%fs(:,3))
      pC%omegatri=gEP%vti/(TG%bigr*mEq%sq%fs(:,3))
      ! Supposedly Eq. B12 of UW-CPTC 09-6R
      coeff=x * (2./2.92) * 1/eps**2
      pC%nustare=coeff * (1./(gEP%tau_e * pc%omegatre))
      pC%nustari=coeff * (1./(gEP%tau_i * pc%omegatri))
      coeff=1.414 /eps**1.5
!      muOVERnu=1.+sqrt(pC%nustare)
      muOVERnu=1.2*x(:)/((1.+sqrt(pC%nustare))*(1.+2.*pC%nustare))
     &        *(1.+1./(gEP%tau_e * pc%omegatre))
c-----------------------------------------------------------------------
c     Calculation of Callen's formulas require calculation of matrices.
c     neomat is the K matrix from table 1 of UW-CPTC-09-6R
c        i=1 => K matrix in banana regime
c        i=2 => K matrix in plateau regime
c        i=3 => K matrix in Pfirsch-Schluter regime
c        i=4 => Total K matrix 
c        i=5 => Viscosity matrix (M)
c        i=6 => Friction matrix (N)
c        i=7 => Inverse(M+N)
c-----------------------------------------------------------------------
      ALLOCATE(neomat(7,0:1,0:1,0:mpsi))
c-----------------------------------------------------------------------
c     Do the ion matrices first matrices to get the ki factor
c       ki = c_sharp mu_i01/mu_i00 Eq. C27 of UW-CPTC-09-06R
c-----------------------------------------------------------------------
      ALLOCATE(csharp(0:mpsi))
      CALL calculate_matrix(zstar,neomat,'ion')
      csharp=1./(1.+(neomat(5,1,1,:)-neomat(5,0,1,:)**2/neomat(5,0,0,:))
     %        /neomat(6,1,1,:))
      pC%ki(3,:)=csharp*neomat(5,0,1,:)/neomat(5,0,0,:)
      DEALLOCATE(csharp)
c-----------------------------------------------------------------------
c     Now do the electron matrices to do most of the work
c     pC%neomat are various neoclassical matrices
c       i=1 => Total K matrix as given by eqn. B13  UW-CPTC-09-6R
c       i=2 => Electron viscosity matrix as given by eqn. C11  UW-CPTC-09-6R
c-----------------------------------------------------------------------
      CALL calculate_matrix(zeff,neomat,'electron')
      pC%neomat(1,:,:,:)=neomat(4,:,:,:)
      pC%neomat(2,:,:,:)=neomat(5,:,:,:)
c-----------------------------------------------------------------------
c     Now calculate the neoclassical parallel resistivity
c-----------------------------------------------------------------------
      pC%etaparnc(3,:)= gEP%eta_prp * 1./(zeff*neomat(7,0,0,:))
c-----------------------------------------------------------------------
c     Calculate the HS values
c-----------------------------------------------------------------------
      ALLOCATE(spfactorHS(0:mpsi))
      spfactorHS=(1.+1.198*zeff*0.222*zeff**2)/
     &           (1.+2.966*zeff*0.750*zeff**2)
      pC%etaparnc(1,:)= gEP%eta_prp * (spfactorHS+muOVERnu)
      DEALLOCATE(spfactorHS)
c-----------------------------------------------------------------------
c     Callen's calculation: See Eq. 17
c     Using a bit of algebra:
c       L31= [(N_e+M_e)-1 M_e]_00
c       L32= [(N_e+M_e)-1 M_e]_01 
c-----------------------------------------------------------------------
      ! This is [(N_e+M_e)-1 M_e]_00
      pC%L31(3,:)=neomat(7,0,0,:)*neomat(5,0,0,:)
     &           +neomat(7,0,1,:)*neomat(5,0,1,:)
      ! This is [(N_e+M_e)-1 M_e]_01
      pC%L32(3,:)=neomat(7,0,0,:)*neomat(5,0,1,:)
     &            +neomat(7,0,1,:)*neomat(5,1,1,:)
      pC%L32(3,:)=-pC%L32(3,:)
      DEALLOCATE(neomat)
c-----------------------------------------------------------------------
c     Hirshman 88 formulas.  What Hirshman calls alpha, we call ki
c-----------------------------------------------------------------------
      ALLOCATE(dd(0:mpsi))
      dd(:)=(      1.414*zstar+   zstar**2)
     &     +(0.754+2.657*zstar+2.*zstar**2)*x(:)
     &     +(0.348+1.243*zstar+   zstar**2)*x(:)**2
      pC%L31(1,:)=(  (0.754+2.21 *zstar+zstar**2)*x(:)
     &         +(0.348+1.243*zstar+zstar**2)*x(:)**2 )/dd(:)
      pC%L32(1,:)=-x(:)*(0.884+2.074*zstar)/dd(:)
      pC%ki(1,:)=1.172/(1.+0.462*x(:))
      DEALLOCATE(dd)
c-----------------------------------------------------------------------
c     Calculate electron and ion temperature  + derivatives
c-----------------------------------------------------------------------
      ALLOCATE(pp(0:mpsi),dpp(0:mpsi),pe(0:mpsi),dpe(0:mpsi))
      ALLOCATE(ti(0:mpsi),dti(0:mpsi),te(0:mpsi),dte(0:mpsi))
      ALLOCATE(dnd(0:mpsi))
      f =>mEq%sq%fs(:,1)
      nd =>mEq%sq%fs(:,5);          dnd = mEq%sq%fs1(:,5)/psio
      pe = mEq%sq%fs(:,6)/mu0;      dpe = mEq%sq%fs1(:,6)/mu0/psio
      pp = mEq%sq%fs(:,2)/mu0-pe;   dpp = mEq%sq%fs1(:,2)/mu0/psio
      ! Note that these are not in eV
      ti = zstar*pp/nd; te = pe/nd
      dti= zstar*(dpp-dpe -ti/zstar*dnd)/nd
      dte= (dpe-te*dnd)/nd
c-----------------------------------------------------------------------
c     SAUTER CALCULATIONS:  O. Sauter et al, Phys. Plasmas 6 (1999) 2834.
c     Program up Equations 14-16
c     Note that I am using zstar instead of zeff. See the discussion
c      after Eq. 12
c     The trickiest part is definition of ki/alpha.  Sauter has:
c       L_34 alpha as the coefficient.  We want this to be L_31 ki
c      so ki = (L_34/L_31) alpha instead of just alpha like Hirshman
c     Sauter says this is to take into account the Zeff dependence
c     Note that I'll use Sauter's calculation of Coulomb log and related
c      formulas even though it's probably more accurate to use the above
c      formulas
c-----------------------------------------------------------------------
      ALLOCATE(nustareS(0:mpsi),clogS(0:mpsi),eN(0:mpsi),fff(0:mpsi))
      ALLOCATE(ft(0:mpsi),ft31eff(0:mpsi),ft32ee_eff(0:mpsi))
      ALLOCATE(ft32ei_eff(0:mpsi),ft34eff(0:mpsi),alpha0(0:mpsi))
      ALLOCATE(sqnue(0:mpsi),l32(0:mpsi),l34(0:mpsi))
      ALLOCATE(sqnui(0:mpsi),xeff(0:mpsi),zp1(0:mpsi),xS(0:mpsi))
      ALLOCATE(nustariS(0:mpsi),clogiS(0:mpsi))

      ! Note that Sauter's formulas here are for eV
      clogS=31.3 - log(sqrt(nd)/gEP%te_ev)
      clogiS=17.34 - log( sqrt( 1e-20*nd/zstar ) / (1e-3*gEP%ti_ev)**1.5 
     &                   * zeff**3 )
      nustareS=6.921E-18 * mEq%sq%fs(:,3)*TG%bigr*nd*zeff*clogS/
     &           (eps**1.5 * gEP%te_ev**2)
      nustariS=4.900e-18 * mEq%sq%fs(:,3) * TG%bigr * nd/zstar * 
     &            zeff**4 * clogiS / (eps**1.5 * gEP%ti_ev**2)
      eN=0.58 + 0.74 /(0.76 + zeff)
      sqnue = sqrt(nustareS) 
      ! This is really sigmaS from first part of 18a of Sauter
      pC%etaparnc(2,:)= 1.9012E4* gEP%te_ev**1.5 /(zeff*eN*nustareS)
      ft=1-pC%fcirc
      xS=ft/(1.+(0.55-0.1*ft)*sqnue+0.45*pC%fcirc*nustareS/zeff**1.5)
      fff=1.-(1.+0.36/zeff)*xS+0.59*xS**2/zeff-0.23*xS**3/zeff
      pC%etaparnc(2,:)=1./(pC%etaparnc(2,:)*fff)
      zp1 = zeff+1.

      ft31eff = ft/ (1.+(1.-0.1*ft)*sqnue+ 0.5*(1.-ft)*nustareS/zeff)
      ft32ee_eff = ft/(1.+0.26*(1.-ft)*sqnue  
     &                       + 0.18*(1.-0.37*ft)*nustareS/sqrt(zeff))
      ft32ei_eff = ft/(1.+(1.+0.6*ft)*sqnue  
     &                       + 0.85*(1.-0.37*ft)*nustareS*zp1)
      ft34eff = ft/(1.+(1.-0.1*ft)*sqnue  
     &                       + 0.5*(1.-0.5*ft)*nustareS/zeff)
!

      xeff=ft31eff
      pC%L31(2,:) = xeff*( (1.+1.4/zp1)
     &            - xeff* (1.9/zp1 - xeff * (0.3/zp1 + 0.2/zp1 * xeff)))

      xeff=ft32ee_eff
      l32 = (0.05+0.62*zeff)/zeff/(1.+0.44*zeff)*(xeff-xeff**4)
     &       +xeff**2/(1.+0.22*zeff)*(1.-1.2*xeff+0.2*xeff**2)
     &       +1.2/(1+0.5*zeff)*xeff**4                                ! 15b

      xeff=ft32ei_eff
      l32 = l32 
     &       -(0.56+1.93*zeff)/zeff/(1.+0.44*zeff)*(xeff-xeff**4)  
     &       +4.95/(1.+2.48*zeff)*xeff**2*(1.-0.55*xeff-0.45*xeff**2)
     &       -1.2/(1+0.5*zeff)*xeff**4                                ! 15c
      pC%L32(2,:)=l32

      xeff=ft34eff
      l34 = xeff*( (1.+1.4/zp1)
     &             - xeff*(1.9/zp1 - xeff * (0.3/zp1 + 0.2/zp1 * xeff)))

      sqnui = sqrt(nustariS)
      xeff = (nustariS**2)*(ft**6)

      alpha0 = - 1.17*(1.-ft)/(1.-0.22*ft-0.19*ft**2)
      alpha0=((alpha0+ 0.25*(1.-ft**2)*sqnui)/(1.+0.5*sqnui)+0.315*xeff)
     &        /(1. + 0.15*xeff)

      pC%ki(2,:)=-alpha0 * l34/pC%L31(2,:)
!      do i=0,mpsi
!       write(*,'(15(e16.8,2x:))') mEq%sq%xs(i), pC%L31(2,i), l32(i), 
!     &    l34(i), alpha0(i), nustariS(i), pc%nustari(i), ft(i),clogiS(i)
!     &    ,  gEP%ti_ev(i)*1e-3, 1e-20*nd(i+1)/ zstar(i), 1e-20*nd(i+1),
!     &       gEP%te_ev(i)*1e-3
!      enddo
      DEALLOCATE(clogS,eN,fff,xS,nustariS,clogiS)
c-----------------------------------------------------------------------
c     Modification to the Sauter formula from parametrization of XGC0
c     computations ( PoP 19 072505 (2012) )
c-----------------------------------------------------------------------
      ALLOCATE(ftcs(0:mpsi)) 
      ALLOCATE(betacs(0:mpsi), deltacs(0:mpsi), zalpha(0:mpsi))
      ALLOCATE(dbananacs(0:mpsi), alphacs(0:mpsi), hfactorcs(0:mpsi))

      betacs = REAL( ( eps - CMPLX(0.44,0) )**0.7 ) 
      WHERE ( zeff > 5. )
        alphacs = 0D0
      ELSEWHERE
        alphacs = ( -zeff**2 + 5.998 * zeff - 4.981 ) /
     &      ( 4.29 * zeff**2 -14.07 * zeff +12.61 )
      END WHERE
      zalpha = zeff**alphacs
      deltacs = 0.55 * zeff**.2 * 
     &  ( TANH( 3.2 * betacs * ( eps**1.5 * nustareS )**1.4 / zalpha )
     &    + ( 1D0 - EXP(-1D1*nustareS) ) *
     &    TANH( 2.2 * betacs * eps**2.8 * nustareS**.1 / zalpha ) )
      dbananacs = TG%bigr * SQRT(eps) * ( -ms(1) / qs(1) ) * gEP%vte
      IF ( ts%edriftToX ) THEN 
        hfactorcs = 1D0 - 0.2/zeff**4 * 
     &     EXP( -ABS( psio * (1.-mEq%sq%xs) / 
     &                ( 2.7 * LOG(eps**1.5 * nustareS/3.2+3D0) 
     &                  * dbananacs) ) )
      ELSE
        hfactorcs = 1D0 - .6/zeff**4 * 
     &     EXP( -ABS( psio * (1.-mEq%sq%xs) / 
     &                ( 3.3 * LOG(eps**1.5*nustareS+2D0) 
     &                  * dbananacs) ) )
      ENDIF
      ftcs = ft * hfactorcs
      ft31eff = ftcs * ( 1. + deltacs ) / 
     &    ( 1.+ (1.-0.1*ftcs) * sqnue+ 0.5 * (1.-ftcs) * nustareS / 
     &     zeff )
      ft32ee_eff = ftcs * ( 1. + deltacs ) / (1.+0.26*(1.-ftcs)*sqnue  
     &                 + 0.18*(1.-0.37*ftcs)*nustareS/sqrt(zeff))
      ft32ei_eff = ftcs * ( 1. + deltacs ) / (1.+(1.+0.6*ftcs)*sqnue  
     &                       + 0.85*(1.-0.37*ftcs)*nustareS*zp1)
      ft34eff = ftcs * ( 1. + deltacs ) / (1.+(1.-0.1*ftcs)*sqnue  
     &                       + 0.5*(1.-0.5*ftcs)*nustareS/zeff)

      xeff=ft31eff
      pC%L31(4,:) = xeff * ( (1.+1.4/zp1)
     &            - xeff * (1.9/zp1 - xeff*(0.3/zp1 + 0.2/zp1 * xeff)))

      xeff=ft32ee_eff
      l32 = (0.05+0.62*zeff)/zeff/(1.+0.44*zeff)*(xeff-xeff**4)
     &       +xeff**2/(1.+0.22*zeff)*(1.-1.2*xeff+0.2*xeff**2)
     &       +1.2/(1+0.5*zeff)*xeff**4                                ! 15b

      xeff=ft32ei_eff
      l32 = l32 
     &       -(0.56+1.93*zeff)/zeff/(1.+0.44*zeff)*(xeff-xeff**4)  
     &       +4.95/(1.+2.48*zeff)*xeff**2*(1.-0.55*xeff-0.45*xeff**2)
     &       -1.2/(1+0.5*zeff)*xeff**4                                ! 15c
      pC%L32(4,:)=l32

      xeff=ft34eff
      l34 = xeff*( (1.+1.4/zp1)
     &             - xeff*(1.9/zp1 - xeff * (0.3/zp1 + 0.2/zp1 * xeff)))
      pC%ki(4,:) = -alpha0 * l34 / pC%L31(4,:) 
      !
      ! etaparnc should be fix in future
      pC%etaparnc(4,:) = pC%etaparnc(2,:)

      DEALLOCATE(ftcs) 
      DEALLOCATE(sqnui)
      DEALLOCATE(ft32ee_eff,ft31eff,ft32ei_eff,ft34eff)
      DEALLOCATE(betacs, deltacs, zalpha)
      DEALLOCATE(alphacs, dbananacs, hfactorcs)
      DEALLOCATE(sqnue,ft,nustareS,xeff,l32,l34,zp1,alpha0)
c-----------------------------------------------------------------------
c     We will express the bootstrap current as:
c        <J.B>_BS=-I[L31 dp/dpsi 
c                  + L32 n_e dT_e/dpsi - L31 ki ne/zz dT_i/dpsi]
c     Here it is important to not have temperature in eV
c-----------------------------------------------------------------------
      ! Hirshman 88
      CALL calculate_jbs(f,pC%L31(1,:),pC%L32(1,:),pC%ki(1,:),
     &                    dpp,dte,dti,nd,zstar,pC%jbs(1,:))
      ! Sauter
      CALL calculate_jbs(f,pC%L31(2,:),pC%L32(2,:),pC%ki(2,:),
     &                    dpp,dte,dti,nd,zstar,pC%jbs(2,:))
      ! Callen
      CALL calculate_jbs(f,pC%L31(3,:),pC%L32(3,:),pC%ki(3,:),
     &                    dpp,dte,dti,nd,zstar,pC%jbs(3,:))
      ! modified Sauter
      CALL calculate_jbs(f,pC%L31(4,:),pC%L32(4,:),pC%ki(4,:),
     &                    dpp,dte,dti,nd,zstar,pC%jbs(4,:))
c-----------------------------------------------------------------------
c     Now calculate current components
c     Calculate the flux-surface average quantities used below
cTMP  - Need to actually do this
c     jdiam returns <j.B>
c     jps   returns <j.B>
c-----------------------------------------------------------------------
      ALLOCATE(bdcp2(0:mpsi),bdct2(0:mpsi),btr2(0:mpsi))
      bdcp2=1.
      bdct2=1.
      btr2=1.
!      quantity = bmod**2/gss
!      CALL fluxav(mEq,quantity,aveh1)               !aveh1 = <B^2/gss>
!      quantity = 1._r8/bmod**2
!      CALL fluxav(mEq,quantity,aveh4)               !aveh4 = <1/B^2>
!      pC%jdia(:) =-gEV%rmean*pp(:)*bdcp2(:)
!      pC%jps(:)  =-gEV%rmean*pp(:)*(bdct2(:)-btr2(:))
      DEALLOCATE(bdcp2,bdct2,btr2)
c-----------------------------------------------------------------------
c     Cleanup
c-----------------------------------------------------------------------
      DEALLOCATE(pp,dpp,pe,dpe,ti,dti,te,dte,zeff,zzi,zstar,x,eps,coeff)
      RETURN
c-----------------------------------------------------------------------
c     neomat is the K matrix from table 1 of UW-CPTC-09-6R
c        First index indicates the type of matrix.
c        i=1 => K matrix in banana regime
c        i=2 => K matrix in plateau regime
c        i=3 => K matrix in Pfirsch-Schluter regime
c        i=4 => Total K matrix in Pfirsch-Schluter regime
c        i=5 => Viscosity matrix (M)
c        i=6 => Friction matrix (N)
c        i=7 => Inverse(M+N)
c-----------------------------------------------------------------------
      CONTAINS
        SUBROUTINE calculate_matrix(zzin,neomat,species)
        REAL(r8), DIMENSION(:), INTENT(IN) :: zzin
        REAL(r8), DIMENSION(:,0:,0:,0:), INTENT(INOUT) :: neomat
        CHARACTER(*), INTENT(IN) :: species
        REAL(r8), DIMENSION(:), ALLOCATABLE :: denom
        REAL(r8), DIMENSION(:,:,:), ALLOCATABLE :: tmat
        REAL(r8), DIMENSION(:), ALLOCATABLE :: omegatau
c-----------------------------------------------------------------------
c       K for the 3 regimes
c-----------------------------------------------------------------------
        ALLOCATE(omegatau(0:mpsi))
        ALLOCATE(denom(0:mpsi))
        neomat(1,0,0,:)=(zzin + 0.533)/zzin
        neomat(1,0,1,:)=(zzin + 0.707)/zzin
        neomat(1,1,0,:)=neomat(1,0,1,:)
        neomat(1,1,1,:)=(2. * zzin + 9./(4.*sqrt(2.)))/zzin
        neomat(2,0,0,:)=1.77
        neomat(2,0,1,:)=5.32
        neomat(2,1,0,:)=neomat(2,0,1,:)
        neomat(2,1,1,:)=21.27
        denom=2.4*zzin**2 + 5.32*zzin + 2.225
        neomat(3,0,0,:)=(4.25*zzin + 3.02)/denom
        neomat(3,0,1,:)=(20.13*zzin + 12.43)/denom
        neomat(3,1,0,:)=neomat(1,0,1,:)
        neomat(3,1,1,:)=(101.06 * zzin + 58.65)/denom
c-----------------------------------------------------------------------
c       neomat are various neoclassical matrices
c         4=1 => Total K matrix as given by eqn. B13  UW-CPTC-09-6R
c         5=2 => Electron viscosity matrix as given by eqn. C11  
c-----------------------------------------------------------------------
        omegatau=zeff * gEP%tau_e * pC%omegatre
        do j=0,1
         do i=0,1
          neomat(4,i,j,:)=neomat(1,i,j,:)/(
     &     (1.+SQRT(pC%nustare)
     &              +2.92*pC%nustare*neomat(1,i,j,:)/neomat(2,i,j,:))
     &    *(1.+2.*neomat(2,i,j,:)/(3.*neomat(3,i,j,:)*omegatau))
     &  )
          enddo
        enddo
c-----------------------------------------------------------------------
c       Viscosity coefficients: Eq. C11 in UW-CPTC-09-06R
c-----------------------------------------------------------------------
        neomat(5,0,0,:)=zzin*x * neomat(4,0,0,:)
        neomat(5,0,1,:)=zzin*x * (5./2.*neomat(4,0,0,:)-neomat(4,0,1,:))
        neomat(5,1,0,:)=neomat(5,0,1,:)
        neomat(5,1,1,:)=zzin*x * 
     &         (neomat(4,1,1,:)-5.*neomat(4,0,1,:)+6.25*neomat(4,0,0,:))
c-----------------------------------------------------------------------
c       Dimensionless Friction: Eq. C9 in UW-CPTC-09-06R
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c       Inverse and factors:  See Eq. C16
c-----------------------------------------------------------------------
        if (trim(species)=='electron') then
          neomat(6,0,0,:)=zzin
          neomat(6,0,1,:)=1.5*zzin; neomat(6,1,0,:)=1.5*zzin
          neomat(6,1,1,:)=sqrt(2.) + 13./4. * zzin
        else
          neomat(6,0,0,:)=0.; neomat(6,0,1,:)=0.; neomat(6,1,0,:)=0.5
          neomat(6,1,1,:)=sqrt(2.) 
        endif
        ! Add in the viscosity coefficent (do not make Jim's ! approximation)
        ALLOCATE(tmat(0:1,0:1,0:mpsi))
        do j=0,1; do i=0,1
           tmat(i,j,:)=neomat(6,i,j,:)+neomat(5,i,j,:)
        enddo; enddo
        ! Inverse M+N
        denom=tmat(0,0,:)*tmat(1,1,:)-tmat(0,1,:)**2
        neomat(7,0,0,:)= tmat(1,1,:)/denom
        neomat(7,0,1,:)=-tmat(1,0,:)/denom
        neomat(7,1,0,:)=-tmat(0,1,:)/denom
        neomat(7,1,1,:)= tmat(0,0,:)/denom
        DEALLOCATE(denom,tmat,omegatau)
        RETURN
        END SUBROUTINE calculate_matrix
c-----------------------------------------------------------------------
c       Bootstrap current
c        <J.B>_BS=-I[L31 dp/dpsi 
c                  + L32 n_e dT_e/dpsi - L31 ki ne/zz dT_i/dpsi]
c-----------------------------------------------------------------------
        SUBROUTINE calculate_jbs(f,L31,L32,ki,dp,dte,dti,nd,zz,jbs)
        REAL(r8), DIMENSION(:), INTENT(IN) :: L31,L32,ki,dp,dte,dti,nd
        REAL(r8), DIMENSION(:), INTENT(IN) :: zz,f
        REAL(r8), DIMENSION(:), INTENT(OUT) :: jbs

        jbs=-f*(L31*dp + L32*nd*dte - L31*ki*nd/zz * dti)

        RETURN
        END SUBROUTINE calculate_jbs
      END SUBROUTINE get_neoclassical
c-----------------------------------------------------------------------
c     subprogram 1. get_transport.
c     computes the geometry quantities often used in transport 
c     calculations
c     Miller parameterizes the surface as:
c        R=R(r) + r cos(theta + arcsin(delta(r)) sin(theta))
c        Z=kappa(r) r sin(theta)
c     This leads to methods for calculating the quantities:
c          r=(R_max-R_min)/2.
c          R=(R_max+R_min)/2.
c          kappa=(Z_max-Z_min)/2/r
c          delta=sin(arccos((R_zmx-R(r))/r)-pi/2)
c     Often, a simpler version of delta is used:
c          delta=(R_zmx-R(r))/r  
c     Also have: alpha=phi + nu(r,theta)
c-----------------------------------------------------------------------
      SUBROUTINE get_transport(gEV,gEP,mEq,TG) !#SIDL_SUB !#SIDL_SUB_NAME=getTransport#
      USE fg_local
      USE fg_physdat
      USE fgAnalyzeTypes
      USE fgMapEqTypes
      IMPLICIT NONE
      TYPE(globalEqDiagnose), INTENT(IN) :: gEV
      TYPE(globalEqProfiles), INTENT(IN) :: gEP
      TYPE(mappedEq), INTENT(IN) :: mEq
      TYPE(TransportGeometry), INTENT(INOUT) :: TG
      REAL(r8) :: r_zsx, rmin, rmax, zmin, zmax, tinynum, psio
      REAL(r8), DIMENSION(4,2) :: rzextrema
      INTEGER(i4) :: ipsi, mpsi, mtheta
      TYPE(spline_type) :: rclc
c-----------------------------------------------------------------------
c     INTERFACE statement
c-----------------------------------------------------------------------
      INTERFACE
        SUBROUTINE find_rzextrema(rzvy,rzextrema)
        USE fg_local
        USE fg_physdat
        USE fg_spline
        IMPLICIT NONE
        REAL(r8), DIMENSION(:,0:), INTENT(IN) :: rzvy
        REAL(r8), DIMENSION(4,2), INTENT(OUT) :: rzextrema
        END SUBROUTINE find_rzextrema
        SUBROUTINE fluxav(mEq,quantity,average)
        USE fg_local
        USE fg_physdat
        USE fgAnalyzeTypes
        USE fgMapEqTypes
        TYPE(mappedEq), INTENT(IN) :: mEq
        REAL(r8), DIMENSION(:,:), INTENT(IN) :: quantity
        REAL(r8), DIMENSION(:), INTENT(OUT) :: average
        END SUBROUTINE fluxav
      END INTERFACE
c-----------------------------------------------------------------------
c     Initializations and allocationgs
c-----------------------------------------------------------------------
      tinynum=SQRT(TINY(tinynum))
      mpsi=mEq%mpsi; mtheta=mEq%mtheta; psio=mEq%psio
      IF(.NOT. ALLOCATED(TG%rho)) CALL transportAlloc(mpsi,TG)
c-----------------------------------------------------------------------
c     Transport codes do everything in terms of toroidal flux, not
c     poloidal flux.  chi=Int(q dpsi)
c     Note that my psio and torfluxo are normalized by 2 pi
c-----------------------------------------------------------------------
      TG%rho=SQRT(gEP%torflux)                          ! Dimensionless
      TG%anorm=SQRT(gEV%torfluxo*twopi/(pi*gEV%bt0))
      TG%rhodim=TG%rho*TG%anorm                         ! Length units
      TG%dpsidchi=gEV%torfluxo/psio/mEq%sq%fs(:,3)      ! Dimensionless
      TG%dpsidrho=2.*TG%rho*TG%dpsidchi                 ! Dimensionless
      TG%dvdrho=gEP%vprime*TG%dpsidrho                  ! Units of Volume
c-----------------------------------------------------------------------
c     Calculate geometric quantities per flux surface
c-----------------------------------------------------------------------
      DO ipsi=0,mpsi
        CALL find_rzextrema(mEq%twod%fs(:,:,ipsi),rzextrema)
        rmin=rzextrema(1,1)
        rmax=rzextrema(2,1)
        zmin=rzextrema(3,2)
        zmax=rzextrema(4,2)
        r_zsx=rzextrema(4,1)
        TG%bigr(ipsi) =(rmax+rmin)/2.
        TG%bigz(ipsi) =(zmax+zmin)/2.
        TG%r(ipsi)    =(rmax-rmin)/2.
        TG%kappa(ipsi)=(zmax-zmin)/2./TG%r(ipsi)
        TG%delta(ipsi)=(TG%bigr(ipsi)-r_zsx)/TG%r(ipsi)
        TG%deltamiller(ipsi)=
     &         SIN(ACOS((r_zsx-TG%bigr(ipsi))/TG%r(ipsi))-pi/2.)
        TG%shafShift(ipsi)=mEq%ro-Tg%bigr(ipsi)
      ENDDO
      CALL fluxav(mEq,mEq%twod%fs(4,:,:)    ,TG%grdpsisq)
      CALL fluxav(mEq,mEq%twod%fs(4,:,:)**.5,TG%grdrho)
      ! mEq%twod%fs is with g^ss with psi units.  Have to take of them
      TG%grdpsisq=TG%grdpsisq/psio**2
      TG%grdrhosq=TG%grdpsisq/(TG%dpsidrho)**2
      TG%grdrho  =TG%grdrho  /(psio*TG%dpsidrho)
      TG%shafAxis=TG%shafShift(0)             
      TG%shafEdge=TG%shafShift(mpsi)   
c-----------------------------------------------------------------------
c     Various gradients
c-----------------------------------------------------------------------
      CALL spline_alloc(rclc,mpsi,6_i4)
      rclc%xs(:)=TG%rho
      rclc%fs(:,1)=mEq%sq%fs(:,3)
      rclc%fs(:,2)=TG%kappa
      rclc%fs(:,3)=TG%delta
      rclc%fs(:,4)=TG%deltamiller
      rclc%fs(:,5)=TG%bigr
      rclc%fs(:,6)=TG%bigz
      CALL spline_fit(rclc,"extrap")
      TG%dqsfdrho   =rclc%fs1(:,1)
      TG%dkappadrho =rclc%fs1(:,2)
      TG%ddeltadrho =rclc%fs1(:,3)
      TG%ddeltamdrho=rclc%fs1(:,4)
      TG%dRmajdrho=rclc%fs1(:,5)
      TG%dZmajdrho=rclc%fs1(:,6)
      CALL spline_dealloc(rclc)
      CALL spline_alloc(rclc,mpsi,1_i4)
      rclc%xs(:)=TG%r
      rclc%fs(:,1)=TG%rho
      CALL spline_fit(rclc,"extrap")
      TG%drhodr=rclc%fs1(:,1)
      CALL spline_dealloc(rclc)
c-----------------------------------------------------------------------
c     Calculate nu
c-----------------------------------------------------------------------
      END SUBROUTINE get_transport

c-----------------------------------------------------------------------
c     subprogram 1. get_thetasf.
c-----------------------------------------------------------------------
      SUBROUTINE get_thetasf(gIn,mEq,thetasf)  !#SIDL_SUB!#SIDL_SUB_NAME=getThetasf#
      USE fg_local
      USE fg_physdat
      USE fgControlTypes
      USE fgMapEqTypes
      IMPLICIT NONE
      TYPE(controlMap), INTENT(IN) :: gIn
      TYPE(mappedEq), INTENT(IN) :: mEq
      TYPE(bicube_type), INTENT(INOUT) :: thetasf
      TYPE(spline_type) :: intgd
      REAL(r8) :: q,f,fprime,jacfac,psio,r
      INTEGER(i4) :: ipsi, itheta, mpsi, mtheta
      mpsi=mEq%mpsi; mtheta=mEq%mtheta; psio=mEq%psio
c-----------------------------------------------------------------------
c     Calculate PEST angle; i.e. theta_sf if zeta_sf=-phi (axisymmetric)
c 	Theta_SF goes from 0 - 2 Pi => Not periodic
c-----------------------------------------------------------------------
      CALL bicube_alloc(thetasf,mtheta,mpsi,1_i4)
      thetasf%xs=mEq%twod%xs
      thetasf%ys=mEq%twod%ys
      CALL spline_alloc(intgd,mtheta,1_i4)
      intgd%xs= mEq%twod%xs                                      ! Theta
      DO ipsi=0,mpsi
       f=mEq%sq%fs(ipsi,1) 
       q=mEq%sq%fs(ipsi,3) 
       fprime=mEq%sq%fs1(ipsi,1) 
       DO itheta=0,mtheta
        r      = mEq%twod%fs(1,itheta,ipsi)
        jacfac = mEq%twod%fs(3,itheta,ipsi)
        intgd%fs(itheta,1)= f/(q*r**2)*jacfac
       ENDDO
       CALL spline_fit(intgd,"extrap")
       CALL spline_int(intgd)
       thetasf%fs(1,:,ipsi)=intgd%fsi(:,1)*2.*pi/intgd%fsi(mtheta,1)
      ENDDO
      CALL bicube_fit(thetasf,"periodic","extrap")
      CALL spline_dealloc(intgd)
      IF(gIn%ipb==0 .AND. gIn%ipr==2 .AND. gIn%angle_method=='jac') THEN
          thetasf%fsx(1,:,:)=1.; thetasf%fsy(1,:,:)=0.
      ENDIF
      RETURN
      END SUBROUTINE get_thetasf
