c-----------------------------------------------------------------------
c     file physdat.f
c     module containing the physical parameters used in the 2-fluid
c     equations.
c     Note: older versions of physdat included zeff, meomi, mtot, and
c     qs(2)=ion charge.  zeff is now an input parameter, and this
c     necessitated moving meomi and mtot to input also. qs(2) was
c     removed to avoid programming errors.  Use qs(2)=-zeff*qs(1)
c     explicitly. These changes required module changes in several files
c     to get the scoping right.  Less elegant but more useful.
c-----------------------------------------------------------------------
      MODULE fg_physdat
      USE fg_local
      IMPLICIT NONE

      REAL(r8), PARAMETER :: pi=3.1415926535897932385_r8
      REAL(r8), PARAMETER :: twopi=2*pi,pisq=pi*pi
      REAL(r8), PARAMETER :: elementary_q=1.60217733e-19_r8
      REAL(r8), PARAMETER, DIMENSION(2) :: 
     $          qs=(/ -elementary_q,elementary_q /)
      REAL(r8), PARAMETER, DIMENSION(2) :: 
     $          ms=(/ 9.1093898e-31_r8, 3.3435860e-27_r8 /)
cc      REAL(r8), PARAMETER, DIMENSION(2) :: 
cc     $          ms=(/ 9.1093898e-31_r8, 1.6726e-27 /)
      REAL(r8), PARAMETER :: clight=2.99792458e8_r8
      REAL(r8), PARAMETER :: mu0=pi*4.e-7_r8 
      REAL(r8), PARAMETER :: eps0=1._r8/(clight**2*mu0)
      REAL(r8), PARAMETER :: gamma=5._r8/3._r8
      REAL(r8), PARAMETER :: gamm1=gamma-1._r8
      REAL(r8), PARAMETER :: kboltz=elementary_q
c-----------------------------------------------------------------------
c     close module
c-----------------------------------------------------------------------
      END MODULE fg_physdat
