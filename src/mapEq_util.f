c-----------------------------------------------------------------------
c     file map_util.f.
c     things useful for inverse.f and direct.f
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c     code organization.
c-----------------------------------------------------------------------
c     0. flder_mod modules
c        1. flder.
c-----------------------------------------------------------------------
      MODULE flder_mod
      USE fg_local
      USE fgInputEqTypes
      IMPLICIT NONE
      TYPE(inputEq), SAVE :: flIn
      INTEGER(i4) :: fldipb, fldipr, fldjpb, fldjpr
      CONTAINS
c-----------------------------------------------------------------------
c     subprogram 2. copyFldMembers
c     flder has to have a particular interface.  Unfortunately, I need
c     access to a inputEq datastructure, and I can't pass it in, so I
c     have to use a common block
c-----------------------------------------------------------------------
      SUBROUTINE copyFldMembers(gIn,eqIn)
      USE fgControlTypes
      IMPLICIT NONE
      TYPE(controlMap), INTENT(IN) :: gIn
      TYPE(inputEq), INTENT(IN) :: eqIn

      fldipb=gIn%ipb
      fldipr=gIn%ipr
      fldjpb=gIn%jpb
      fldjpr=gIn%jpr
      flIn%ro=eqIn%ro
      flIn%zo=eqIn%zo
      flIn%psio=eqIn%psio
      flIn%psio=eqIn%psio
      flIn%mx=eqIn%mx; flIn%my=eqIn%my; flIn%ms_in=eqIn%ms_in
      CALL bicube_alloc(flIn%psi_in,flIn%mx,flIn%my,1_i4)
      flIn%psi_in%xs=eqIn%psi_in%xs
      flIn%psi_in%ys=eqIn%psi_in%ys
      flIn%psi_in%fs=eqIn%psi_in%fs
      CALL bicube_fit(flIn%psi_in,"extrap","extrap")
      CALL spline_alloc(flIn%sq_in,flIn%ms_in,flIn%nsq_in)
      flIn%sq_in%xs=eqIn%sq_in%xs
      flIn%sq_in%fs=eqIn%sq_in%fs
      CALL spline_fit(flIn%sq_in,"extrap")

      RETURN
      END SUBROUTINE copyFldMembers
 
c-----------------------------------------------------------------------
c     subprogram 4. flder.
c     contains differential equations for field line averages.
c                 y(1) --> transformed angle
c                 y(2) --> effective radial coordinate
c                 y(3) --> 1/R^2 (used to calculate q)
c                 y(4) --> packing angle
c-----------------------------------------------------------------------
      SUBROUTINE flder(neq,eta,y,dy)
      USE fg_local
      IMPLICIT NONE
      
      INTEGER(i4), INTENT(IN) :: neq
      REAL(r8), INTENT(IN) :: eta
      REAL(r8), INTENT(IN) :: y(neq)
      REAL(r8), INTENT(OUT) :: dy(neq)

      REAL(r8) :: cosfac,sinfac,bp,r,z
c-----------------------------------------------------------------------
c     preliminary computations.
c-----------------------------------------------------------------------
      cosfac=COS(eta)
      sinfac=SIN(eta)
      r=flIn%ro+y(2)*cosfac
      z=flIn%zo+y(2)*sinfac
      CALL get_bfield(r,z,flIn,1_i4)
      bp=SQRT(flIn%bf%br**2+flIn%bf%bz**2)
c-----------------------------------------------------------------------
c     compute derivatives.
c-----------------------------------------------------------------------
      dy(1)=y(2)/(flIn%bf%bz*cosfac-flIn%bf%br*sinfac)
      dy(2)=dy(1)*(flIn%bf%br*cosfac+flIn%bf%bz*sinfac)
      dy(3)=dy(1)/(r*r)
      dy(4)=dy(1)*bp**fldjpb/r**fldjpr
      dy(1)=dy(1)*bp**fldipb/r**fldipr
c-----------------------------------------------------------------------
c     terminate.
c-----------------------------------------------------------------------
      RETURN
      END SUBROUTINE flder
      END MODULE
c-----------------------------------------------------------------------
c     subprogram 2. copyMembers
c     For convenience, it is necessary to start off with having the 
c     mapped type have the same values from the eqIn type so that the 
c     mapped type keep track of the original values, although
c     some of the values here can be revised later
c-----------------------------------------------------------------------
      SUBROUTINE copyMembers(gIn,eqIn,mEq)
      USE fg_local
      USE fgControlTypes
      USE fgInputEqTypes
      USE fgMapEqTypes
      IMPLICIT NONE
      TYPE(controlMap), INTENT(IN) :: gIn
      TYPE(inputEq), INTENT(IN) :: eqIn
      TYPE(mappedEq), INTENT(OUT) :: mEq

      mEq%mpsi=gIn%mpsi
      mEq%mtheta=gIn%mtheta
      mEq%mvac=gIn%mvac
      mEq%zz=eqIn%zz
      mEq%ro=eqIn%ro
      mEq%zo=eqIn%zo
      mEq%psio=eqIn%psio
      mEq%direct_flag=0

      RETURN
      END SUBROUTINE copyMembers
 
c-----------------------------------------------------------------------
c     subprogram 2. write_2d.
c     produces ascii and binary output for R,Z(tau,a).
c-----------------------------------------------------------------------
      SUBROUTINE write_2d(gIn,mEq,r2g,io)
      USE fgControlTypes
      USE fgMapEqTypes
      IMPLICIT NONE
      TYPE(controlMap), INTENT(IN) :: gIn
      TYPE(mappedEq), INTENT(IN) :: mEq
      TYPE(bicube_type), INTENT(IN) :: r2g
      TYPE(fgControlIO), INTENT(IN) :: io

      INTEGER, PARAMETER :: iua=40
      INTEGER, PARAMETER :: iub=41
      INTEGER(i4) :: ipsi,itau
      REAL(r8) :: tau,deta,eta,r2,psi,r,z
      INTEGER(i4) :: mvac,mpsi,mtheta,mxpie
c-----------------------------------------------------------------------
c     WRITE formats.
c-----------------------------------------------------------------------
 2010 FORMAT(1x,'ipsi = ',i3,', psi = ',1p,e11.3)
 2015 FORMAT(1x,'ipsi = ',i3,', jpsi = ',i1,', psi = ',1p,e11.3)
 2020 FORMAT(/4x,'it',5x,'tau',9x,'r2',8x,'deta',7x,'eta',9x,'r',10x,
     $     'z'/)
 2030 FORMAT(i6,1p,6e11.3)
      mvac=mEq%mvac; mpsi=mEq%mpsi; mtheta=mEq%mtheta
c-----------------------------------------------------------------------
c     open output files.
c-----------------------------------------------------------------------
      IF(io%out_2d)OPEN(UNIT=iua,FILE='2d.txt',STATUS='UNKNOWN')
      IF(io%bin_2d)CALL open_bin(iub,"2d.bin","UNKNOWN","REWIND",32_i4)
c-----------------------------------------------------------------------
c     write input data along flux surfaces.
c-----------------------------------------------------------------------
      IF(io%out_2d .OR. io%bin_2d)THEN
       IF(io%out_2d)WRITE(iua,'(1x,a/)')"input data"
       DO ipsi=0,mpsi
          psi=(1.-mEq%twod%ys(ipsi))*mEq%psio
          IF(io%out_2d)THEN
             WRITE(iua,2010)ipsi,psi
             WRITE(iua,2020)
          ENDIF
          DO itau=0,mtheta
             tau=mEq%twod%xs(itau)
             deta=r2g%fs(2,itau,ipsi)
             eta=tau+deta
             IF(gIn%calc_direct) THEN
               r2=mEq%dir%fs(14,itau,ipsi)
             ELSE
               r2=r2g%fs(1,itau,ipsi)
             ENDIF
             r=mEq%twod%fs(1,itau,ipsi)
             z=mEq%twod%fs(2,itau,ipsi)
             IF(io%out_2d)WRITE(iua,2030)itau,tau,r2,deta,eta,r,z
             IF(io%bin_2d)WRITE(iub)(/REAL(tau,4),REAL(psi,4),
     $         REAL(r2,4),REAL(deta,4),REAL(eta,4),REAL(r,4),REAL(z,4)/)
          ENDDO
          IF(io%out_2d)WRITE(iua,2020)
          IF(io%bin_2d)WRITE(iub)
       ENDDO
       DO ipsi=mpsi+1,mpsi+mvac
          IF(io%out_2d)THEN
             WRITE(iua,2010)ipsi,(mEq%dir%fs(14,0,ipsi))**2
             WRITE(iua,2020)
          ENDIF
          DO itau=0,mtheta
             r=mEq%dir%fs(1,itau,ipsi)
             z=mEq%dir%fs(2,itau,ipsi)
c            WRITE(*,*) r,z
             tau=mEq%dir%xs(itau)
             deta=r2g%fs(2,itau,mpsi)
             eta=tau+deta
             psi=mEq%dir%fs(13,itau,ipsi)
             r2=mEq%dir%fs(14,itau,ipsi)
             IF(io%out_2d)WRITE(iua,2030)itau,tau,r2,deta,eta,r,z
             IF(io%bin_2d)WRITE(iub)(/REAL(tau,4),REAL(psi,4),
     $         REAL(r2,4),REAL(deta,4),REAL(eta,4),REAL(r,4),REAL(z,4)/)
          ENDDO
          IF(io%out_2d)WRITE(iua,2020)
          IF(io%bin_2d)WRITE(iub)
       ENDDO
      ENDIF
      IF(io%bin_2d)CALL close_bin(iub,"2d.bin")
c-----------------------------------------------------------------------
c      Do another version of this
c-----------------------------------------------------------------------
c      IF(bin_2d) THEN
c        CALL open_bin(iub,"2d_1d.bin","UNKNOWN","REWIND",32_i4)
c        DO itau=0,mtheta
c          DO ipsi=0,mpsi+mvac
c            r=mEq%dir%fs(1,itau,ipsi)
c            z=mEq%dir%fs(2,itau,ipsi)
c            psi=mEq%dir%fs(13,itau,ipsi)/mEq%psio
c            r2=mEq%dir%fs(14,itau,ipsi)
c            WRITE(iub) (/REAL(ipsi,4)/mpsi,REAL(itau,4)/mtheta,
c     $        REAL(psi,4),REAL(r2,4),REAL(r,4),REAL(z,4)/)
c          ENDDO
c          WRITE(iub)
c       ENDDO
c       CALL close_bin(binary_unit,'2d_1d.bin')
c      ENDIF
c-----------------------------------------------------------------------
      RETURN
      END SUBROUTINE write_2d

