      REAL*8 FUNCTION DVNORM (N, V, W) 
      IMPLICIT NONE
!***BEGIN PROLOGUE  DVNORM                                              
!***SUBSIDIARY                                                          
!***PURPOSE  Weighted root-mean-square vector norm.                     
!***LIBRARY   MATHLIB (ODEPACK)                                         
!***TYPE      REAL*8 (SVNORM-S, DVNORM-D)                               
!***AUTHOR  Hindmarsh, Alan C., (LLNL)                                  
!***DESCRIPTION                                                         
!                                                                       
!  This function routine computes the weighted root-mean-square norm    
!  of the vector of length N contained in the array V, with weights     
!  contained in the array W of length N:                                
!    DVNORM = SQRT( (1/N) * SUM( V(i)*W(i) )**2 )                       
!                                                                       
!***SEE ALSO  LSODE                                                     
!***ROUTINES CALLED  (NONE)                                             
!***REVISION HISTORY  (YYMMDD)                                          
!   791129  DATE WRITTEN                                                
!   890501  Modified prologue to SLATEC/LDOC format.  (FNF)             
!   890503  Minor cosmetic changes.  (FNF)                              
!   930809  Renamed to allow single/real*8 versions. (ACH)              
!***END PROLOGUE  DVNORM                                                
!**End                                                                  
      INTEGER N,   I 
      REAL*8 V, W,   SUM 
      DIMENSION V(N), W(N) 
!                                                                       
!***FIRST EXECUTABLE STATEMENT  DVNORM                                  
      SUM = 0.0 
      DO 10 I = 1,N 
   10   SUM = SUM + (V(I)*W(I))**2 
      DVNORM = SQRT(SUM/N) 
      RETURN 
!----------------------- END OF FUNCTION DVNORM ------------------------
      END                                           
