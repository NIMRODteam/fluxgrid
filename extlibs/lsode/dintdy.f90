      SUBROUTINE DINTDY (T, K, YH, NYH, DKY, IFLAG) 
      implicit none
!***BEGIN PROLOGUE  DINTDY                                              
!***SUBSIDIARY                                                          
!***PURPOSE  Interpolate solution derivatives.                          
!***LIBRARY   MATHLIB (ODEPACK)                                         
!***TYPE      REAL*8 (SINTDY-S, DINTDY-D)                               
!***AUTHOR  Hindmarsh, Alan C., (LLNL)                                  
!***DESCRIPTION                                                         
!                                                                       
!  DINTDY computes interpolated values of the K-th derivative of the    
!  dependent variable vector y, and stores it in DKY.  This routine     
!  is called within the package with K = 0 and T = TOUT, but may        
!  also be called by the user for any K up to the current order.        
!  (See detailed instructions in the usage documentation.)              
!                                                                       
!  The computed values in DKY are gotten by interpolation using the     
!  Nordsieck history array YH.  This array corresponds uniquely to a    
!  vector-valued polynomial of degree NQCUR or less, and DKY is set     
!  to the K-th derivative of this polynomial at T.                      
!  The formula for DKY is:                                              
!               q                                                       
!   DKY(i)  =  sum  c(j,K) * (T - tn)**(j-K) * h**(-j) * YH(i,j+1)      
!              j=K                                                      
!  where  c(j,K) = j*(j-1)*...*(j-K+1), q = NQCUR, tn = TCUR, h = HCUR. 
!  The quantities  nq = NQCUR, l = nq+1, N = NEQ, tn, and h are         
!  communicated by COMMON.  The above sum is done in reverse order.     
!  IFLAG is returned negative if either K or T is out of bounds.        
!                                                                       
!***SEE ALSO  LSODE                                                     
!***ROUTINES CALLED  XERRWD                                             
!***COMMON BLOCKS    DLS001                                             
!***REVISION HISTORY  (YYMMDD)                                          
!   791129  DATE WRITTEN                                                
!   890501  Modified prologue to SLATEC/LDOC format.  (FNF)             
!   890503  Minor cosmetic changes.  (FNF)                              
!   930809  Renamed to allow single/real*8 versions. (ACH)              
!***END PROLOGUE  DINTDY                                                
!**End                                                                  
      INTEGER K, NYH, IFLAG 
      INTEGER IOWND, IOWNS,                                             &
     &   ICF, IERPJ, IERSL, JCUR, JSTART, KFLAG, L, METH, MITER,        &
     &   MAXORD, MAXCOR, MSBP, MXNCF, N, NQ, NST, NFE, NJE, NQU         
      INTEGER I, IC, J, JB, JB2, JJ, JJ1, JP1 
      REAL*8 T, YH, DKY 
      REAL*8 ROWNS,                                                     &
     &   CCMAX, EL0, H, HMIN, HMXI, HU, RC, TN, UROUND                  
      REAL*8 C, R, S, TP 
      CHARACTER*80 MSG 
      DIMENSION YH(NYH,*), DKY(*) 
      COMMON /DLS001/ ROWNS(209),                                       &
     &   CCMAX, EL0, H, HMIN, HMXI, HU, RC, TN, UROUND,                 &
     &   IOWND(12), IOWNS(6),                                           &
     &   ICF, IERPJ, IERSL, JCUR, JSTART, KFLAG, L, METH, MITER,        &
     &   MAXORD, MAXCOR, MSBP, MXNCF, N, NQ, NST, NFE, NJE, NQU         
      !$omp threadprivate(/DLS001/)
!                                                                       
!***FIRST EXECUTABLE STATEMENT  DINTDY                                  
      IFLAG = 0 
      IF (K < 0 .OR. K > NQ) GO TO 80 
      TP = TN - HU -  100.0*UROUND*(TN + HU) 
      IF ((T-TP)*(T-TN) > 0.0) GO TO 90 
!                                                                       
      S = (T - TN)/H 
      IC = 1 
      IF (K == 0) GO TO 15 
      JJ1 = L - K 
      DO 10 JJ = JJ1,NQ 
   10   IC = IC*JJ 
   15 C = IC 
      DO 20 I = 1,N 
   20   DKY(I) = C*YH(I,L) 
      IF (K == NQ) GO TO 55 
      JB2 = NQ - K 
      DO 50 JB = 1,JB2 
        J = NQ - JB 
        JP1 = J + 1 
        IC = 1 
        IF (K == 0) GO TO 35 
        JJ1 = JP1 - K 
        DO 30 JJ = JJ1,J 
   30     IC = IC*JJ 
   35   C = IC 
        DO 40 I = 1,N 
   40     DKY(I) = C*YH(I,JP1) + S*DKY(I) 
   50   CONTINUE 
      IF (K == 0) RETURN 
   55 R = H**(-K) 
      DO 60 I = 1,N 
   60   DKY(I) = R*DKY(I) 
      RETURN 
!                                                                       
   80 MSG = 'DINTDY-  K (=I1) illegal      ' 
      CALL XERRWD (MSG, 30, 51, 0, 1, K, 0, 0, 0._8, 0._8) 
      IFLAG = -1 
      RETURN 
   90 MSG = 'DINTDY-  T (=R1) illegal      ' 
      CALL XERRWD (MSG, 30, 52, 0, 0, 0, 0, 1, T, 0._8) 
      MSG='      T not in interval TCUR - HU (= R1) to TCUR (=R2)      ' 
      CALL XERRWD (MSG, 60, 52, 0, 0, 0, 0, 2, TP, TN) 
      IFLAG = -2 
      RETURN 
!----------------------- END OF SUBROUTINE DINTDY ----------------------
      END                                           
