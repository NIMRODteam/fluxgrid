      INTEGER FUNCTION IUMACH() 
      implicit none
!***BEGIN PROLOGUE  IUMACH                                              
!***PURPOSE  Provide standard output unit number.                       
!***LIBRARY   MATHLIB                                                   
!***CATEGORY  R1                                                        
!***TYPE      INTEGER (IUMACH-I)                                        
!***KEYWORDS  MACHINE CONSTANTS                                         
!***AUTHOR  Hindmarsh, Alan C., (LLNL)                                  
!***DESCRIPTION                                                         
! *Usage:                                                               
!        INTEGER  LOUT, IUMACH                                          
!        LOUT = IUMACH()                                                
!                                                                       
! *Function Return Values:                                              
!     LOUT : the standard logical unit for Fortran output.              
!                                                                       
!***REFERENCES  (NONE)                                                  
!***ROUTINES CALLED  (NONE)                                             
!***REVISION HISTORY  (YYMMDD)                                          
!   930915  DATE WRITTEN                                                
!   930922  Made user-callable, and other cosmetic changes. (FNF)       
!***END PROLOGUE  IUMACH                                                
!                                                                       
!*Internal Notes:                                                       
!  The built-in value of 6 is standard on a wide range of Fortran       
!  systems.  This may be machine-dependent.                             
!**End                                                                  
!***FIRST EXECUTABLE STATEMENT  IUMACH                                  
      IUMACH = 6 
!                                                                       
      RETURN 
!----------------------- End of Function IUMACH ------------------------
      END                                           
