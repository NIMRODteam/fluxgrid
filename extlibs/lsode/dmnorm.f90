      REAL*8 FUNCTION DMNORM (N, V, W)
      IMPLICIT NONE
!-----------------------------------------------------------------------
! This function routine computes the weighted max-norm
! of the vector of length N contained in the array V, with weights
! contained in the array w of length N:
!   DMNORM = MAX(i=1,...,N) ABS(V(i))*W(i)
!-----------------------------------------------------------------------
      INTEGER N,   I
      REAL*8 V, W,   VM
      DIMENSION V(N), W(N)
      VM = 0.0
      DO 10 I = 1,N
   10   VM = MAX(VM,ABS(V(I))*W(I))
      DMNORM = VM
      RETURN
!----------------------- End of Function DMNORM ------------------------
      END