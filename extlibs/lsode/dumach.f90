      REAL*8 FUNCTION DUMACH () 
      implicit none
!***BEGIN PROLOGUE  DUMACH                                              
!***PURPOSE  Compute the unit roundoff of the machine.                  
!***LIBRARY   MATHLIB                                                   
!***CATEGORY  R1                                                        
!***TYPE      REAL*8 (RUMACH-S, DUMACH-D)                               
!***KEYWORDS  MACHINE CONSTANTS                                         
!***AUTHOR  Hindmarsh, Alan C., (LLNL)                                  
!***DESCRIPTION                                                         
! *Usage:                                                               
!        REAL*8  A, DUMACH                                              
!        A = DUMACH()                                                   
!                                                                       
! *Function Return Values:                                              
!     A : the unit roundoff of the machine.                             
!                                                                       
! *Description:                                                         
!     The unit roundoff is defined as the smallest positive machine     
!     number u such that  1.0 + u .ne. 1.0.  This is computed by DUMACH 
!     in a machine-independent manner.                                  
!                                                                       
!***REFERENCES  (NONE)                                                  
!***ROUTINES CALLED  (NONE)                                             
!***REVISION HISTORY  (YYMMDD)                                          
!   930216  DATE WRITTEN                                                
!   930818  Added SLATEC-format prologue.  (FNF)                        
!***END PROLOGUE  DUMACH                                                
!                                                                       
!*Internal Notes:                                                       
!-----------------------------------------------------------------------
! Subroutines/functions called by DUMACH.. None                         
!-----------------------------------------------------------------------
!**End                                                                  
!                                                                       
      REAL*8 U, COMP 
!***FIRST EXECUTABLE STATEMENT  DUMACH                                  
      U = 1.0 
   10 U = U*0.5 
      COMP = 1.0 + U 
      IF (COMP /= 1.0) GO TO 10 
      DUMACH = U*2.0 
      RETURN 
!----------------------- End of Function DUMACH ------------------------
      END                                           
