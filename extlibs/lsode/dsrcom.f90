      SUBROUTINE DSRCOM (RSAV, ISAV, JOB) 
      IMPLICIT NONE
!***BEGIN PROLOGUE  DSRCOM                                              
!***SUBSIDIARY                                                          
!***PURPOSE  Save/restore ODEPACK COMMON blocks.                        
!***LIBRARY   MATHLIB (ODEPACK)                                         
!***TYPE      REAL*8 (SSRCOM-S, DSRCOM-D)                               
!***AUTHOR  Hindmarsh, Alan C., (LLNL)                                  
!***DESCRIPTION                                                         
!                                                                       
!  This routine saves or restores (depending on JOB) the contents of    
!  the COMMON block DLS001, which is used internally                    
!  by one or more ODEPACK solvers.                                      
!                                                                       
!  RSAV = real array of length 218 or more.                             
!  ISAV = integer array of length 37 or more.                           
!  JOB  = flag indicating to save or restore the COMMON blocks:         
!         JOB  = 1 if COMMON is to be saved (written to RSAV/ISAV)      
!         JOB  = 2 if COMMON is to be restored (read from RSAV/ISAV)    
!         A call with JOB = 2 presumes a prior call with JOB = 1.       
!                                                                       
!***SEE ALSO  LSODE                                                     
!***ROUTINES CALLED  (NONE)                                             
!***COMMON BLOCKS    DLS001                                             
!***REVISION HISTORY  (YYMMDD)                                          
!   791129  DATE WRITTEN                                                
!   890501  Modified prologue to SLATEC/LDOC format.  (FNF)             
!   890503  Minor cosmetic changes.  (FNF)                              
!   921116  Deleted treatment of block /EH0001/.  (ACH)                 
!   930801  Reduced Common block length by 2.  (ACH)                    
!   930809  Renamed to allow single/real*8 versions. (ACH)              
!***END PROLOGUE  DSRCOM                                                
!**End                                                                  
      INTEGER ISAV, JOB 
      INTEGER ILS 
      INTEGER I, LENILS, LENRLS 
      REAL*8 RSAV,   RLS 
      DIMENSION RSAV(*), ISAV(*) 
      COMMON /DLS001/ RLS(218), ILS(37) 
      !$omp threadprivate(/DLS001/)
      DATA LENRLS/218/, LENILS/37/ 
!                                                                       
!***FIRST EXECUTABLE STATEMENT  DSRCOM                                  
      IF (JOB == 2) GO TO 100 
!                                                                       
      DO 10 I = 1,LENRLS 
   10   RSAV(I) = RLS(I) 
      DO 20 I = 1,LENILS 
   20   ISAV(I) = ILS(I) 
      RETURN 
!                                                                       
  100 CONTINUE 
      DO 110 I = 1,LENRLS 
  110    RLS(I) = RSAV(I) 
      DO 120 I = 1,LENILS 
  120    ILS(I) = ISAV(I) 
      RETURN 
!----------------------- END OF SUBROUTINE DSRCOM ----------------------
      END                                           
