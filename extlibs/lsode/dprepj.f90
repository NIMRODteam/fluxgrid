      SUBROUTINE DPREPJ (NEQ, Y, YH, NYH, EWT, FTEM, SAVF, WM, IWM,     &
     &   F, JAC)                                                        
      IMPLICIT NONE
!***BEGIN PROLOGUE  DPREPJ                                              
!***SUBSIDIARY                                                          
!***PURPOSE  Compute and process Newton iteration matrix.               
!***LIBRARY   MATHLIB (ODEPACK)                                         
!***TYPE      REAL*8 (SPREPJ-S, DPREPJ-D)                               
!***AUTHOR  Hindmarsh, Alan C., (LLNL)                                  
!***DESCRIPTION                                                         
!                                                                       
!  DPREPJ is called by DSTODE to compute and process the matrix         
!  P = I - h*el(1)*J , where J is an approximation to the Jacobian.     
!  Here J is computed by the user-supplied routine JAC if               
!  MITER = 1 or 4, or by finite differencing if MITER = 2, 3, or 5.     
!  If MITER = 3, a diagonal approximation to J is used.                 
!  J is stored in WM and replaced by P.  If MITER .ne. 3, P is then     
!  subjected to LU decomposition in preparation for later solution      
!  of linear systems with P as coefficient matrix.  This is done        
!  by DGEFA if MITER = 1 or 2, and by DGBFA if MITER = 4 or 5.          
!                                                                       
!  In addition to variables described in DSTODE and LSODE prologues,    
!  communication with DPREPJ uses the following:                        
!  Y     = array containing predicted values on entry.                  
!  FTEM  = work array of length N (ACOR in DSTODE).                     
!  SAVF  = array containing f evaluated at predicted y.                 
!  WM    = real work space for matrices.  On output it contains the     
!          inverse diagonal matrix if MITER = 3 and the LU decomposition
!          of P if MITER is 1, 2 , 4, or 5.                             
!          Storage of matrix elements starts at WM(3).                  
!          WM also contains the following matrix-related data:          
!          WM(1) = SQRT(UROUND), used in numerical Jacobian increments. 
!          WM(2) = H*EL0, saved for later use if MITER = 3.             
!  IWM   = integer work space containing pivot information, starting at 
!          IWM(21), if MITER is 1, 2, 4, or 5.  IWM also contains band  
!          parameters ML = IWM(1) and MU = IWM(2) if MITER is 4 or 5.   
!  EL0   = EL(1) (input).                                               
!  IERPJ = output error flag,  = 0 if no trouble, .gt. 0 if             
!          P matrix found to be singular.                               
!  JCUR  = output flag = 1 to indicate that the Jacobian matrix         
!          (or approximation) is now current.                           
!  This routine also uses the COMMON variables EL0, H, TN, UROUND,      
!  MITER, N, NFE, and NJE.                                              
!                                                                       
!***SEE ALSO  LSODE                                                     
!***ROUTINES CALLED  DGBFA, DGEFA, DVNORM                               
!***COMMON BLOCKS    DLS001                                             
!***REVISION HISTORY  (YYMMDD)                                          
!   791129  DATE WRITTEN                                                
!   890501  Modified prologue to SLATEC/LDOC format.  (FNF)             
!   890504  Minor cosmetic changes.  (FNF)                              
!   930809  Renamed to allow single/real*8 versions. (ACH)              
!***END PROLOGUE  DPREPJ                                                
!**End                                                                  
      EXTERNAL F, JAC 
      INTEGER NEQ, NYH, IWM 
      INTEGER IOWND, IOWNS,                                             &
     &   ICF, IERPJ, IERSL, JCUR, JSTART, KFLAG, L, METH, MITER,        &
     &   MAXORD, MAXCOR, MSBP, MXNCF, N, NQ, NST, NFE, NJE, NQU         
      INTEGER I, I1, I2, IER, II, J, J1, JJ, LENP,                      &
     &   MBA, MBAND, MEB1, MEBAND, ML, ML3, MU, NP1                     
      REAL*8 Y, YH, EWT, FTEM, SAVF, WM 
      REAL*8 ROWNS,                                                     &
     &   CCMAX, EL0, H, HMIN, HMXI, HU, RC, TN, UROUND                  
      REAL*8 CON, DI, FAC, HL0, R, R0, SRUR, YI, YJ, YJJ,               &
     &   DVNORM                                                         
      DIMENSION NEQ(*), Y(*), YH(NYH,*), EWT(*), FTEM(*), SAVF(*),      &
     &   WM(*), IWM(*)                                                  
      COMMON /DLS001/ ROWNS(209),                                       &
     &   CCMAX, EL0, H, HMIN, HMXI, HU, RC, TN, UROUND,                 &
     &   IOWND(12), IOWNS(6),                                           &
     &   ICF, IERPJ, IERSL, JCUR, JSTART, KFLAG, L, METH, MITER,        &
     &   MAXORD, MAXCOR, MSBP, MXNCF, N, NQ, NST, NFE, NJE, NQU         
      !$omp threadprivate(/DLS001/)
!                                                                       
!***FIRST EXECUTABLE STATEMENT  DPREPJ                                  
      NJE = NJE + 1 
      IERPJ = 0 
      JCUR = 1 
      HL0 = H*EL0 
      GO TO (100, 200, 300, 400, 500), MITER 
! If MITER = 1, call JAC and multiply by scalar. -----------------------
  100 LENP = N*N 
      DO 110 I = 1,LENP 
  110   WM(I+2) = 0.0 
      CALL JAC (NEQ, TN, Y, 0, 0, WM(3), N) 
      CON = -HL0 
      DO 120 I = 1,LENP 
  120   WM(I+2) = WM(I+2)*CON 
      GO TO 240 
! If MITER = 2, make N calls to F to approximate J. --------------------
  200 FAC = DVNORM (N, SAVF, EWT) 
      R0 = 1000.0*ABS(H)*UROUND*N*FAC 
      IF (R0 == 0.0) R0 = 1.0 
      SRUR = WM(1) 
      J1 = 2 
      DO 230 J = 1,N 
        YJ = Y(J) 
        R = MAX(SRUR*ABS(YJ),R0/EWT(J)) 
        Y(J) = Y(J) + R 
        FAC = -HL0/R 
        CALL F (NEQ, TN, Y, FTEM) 
        DO 220 I = 1,N 
  220     WM(I+J1) = (FTEM(I) - SAVF(I))*FAC 
        Y(J) = YJ 
        J1 = J1 + N 
  230   CONTINUE 
      NFE = NFE + N 
! Add identity matrix. -------------------------------------------------
  240 J = 3 
      NP1 = N + 1 
      DO 250 I = 1,N 
        WM(J) = WM(J) + 1.0 
  250   J = J + NP1 
! Do LU decomposition on P. --------------------------------------------
      CALL DGEFA (WM(3), N, N, IWM(21), IER) 
      IF (IER /= 0) IERPJ = 1 
      RETURN 
! If MITER = 3, construct a diagonal approximation to J and P. ---------
  300 WM(2) = HL0 
      R = EL0*0.1 
      DO 310 I = 1,N 
  310   Y(I) = Y(I) + R*(H*SAVF(I) - YH(I,2)) 
      CALL F (NEQ, TN, Y, WM(3)) 
      NFE = NFE + 1 
      DO 320 I = 1,N 
        R0 = H*SAVF(I) - YH(I,2) 
        DI = 0.1*R0 - H*(WM(I+2) - SAVF(I)) 
        WM(I+2) = 1.0 
        IF (ABS(R0) < UROUND/EWT(I)) GO TO 320 
        IF (ABS(DI) == 0.0) GO TO 330 
        WM(I+2) = 0.1*R0/DI 
  320   CONTINUE 
      RETURN 
  330 IERPJ = 1 
      RETURN 
! If MITER = 4, call JAC and multiply by scalar. -----------------------
  400 ML = IWM(1) 
      MU = IWM(2) 
      ML3 = ML + 3 
      MBAND = ML + MU + 1 
      MEBAND = MBAND + ML 
      LENP = MEBAND*N 
      DO 410 I = 1,LENP 
  410   WM(I+2) = 0.0 
      CALL JAC (NEQ, TN, Y, ML, MU, WM(ML3), MEBAND) 
      CON = -HL0 
      DO 420 I = 1,LENP 
  420   WM(I+2) = WM(I+2)*CON 
      GO TO 570 
! If MITER = 5, make MBAND calls to F to approximate J. ----------------
  500 ML = IWM(1) 
      MU = IWM(2) 
      MBAND = ML + MU + 1 
      MBA = MIN(MBAND,N) 
      MEBAND = MBAND + ML 
      MEB1 = MEBAND - 1 
      SRUR = WM(1) 
      FAC = DVNORM (N, SAVF, EWT) 
      R0 = 1000.0*ABS(H)*UROUND*N*FAC 
      IF (R0 == 0.0) R0 = 1.0 
      DO 560 J = 1,MBA 
        DO 530 I = J,N,MBAND 
          YI = Y(I) 
          R = MAX(SRUR*ABS(YI),R0/EWT(I)) 
  530     Y(I) = Y(I) + R 
        CALL F (NEQ, TN, Y, FTEM) 
        DO 550 JJ = J,N,MBAND 
          Y(JJ) = YH(JJ,1) 
          YJJ = Y(JJ) 
          R = MAX(SRUR*ABS(YJJ),R0/EWT(JJ)) 
          FAC = -HL0/R 
          I1 = MAX(JJ-MU,1) 
          I2 = MIN(JJ+ML,N) 
          II = JJ*MEB1 - ML + 2 
          DO 540 I = I1,I2 
  540       WM(II+I) = (FTEM(I) - SAVF(I))*FAC 
  550     CONTINUE 
  560   CONTINUE 
      NFE = NFE + MBA 
! Add identity matrix. -------------------------------------------------
  570 II = MBAND + 2 
      DO 580 I = 1,N 
        WM(II) = WM(II) + 1.0 
  580   II = II + MEBAND 
! Do LU decomposition of P. --------------------------------------------
      CALL DGBFA (WM(3), MEBAND, N, ML, MU, IWM(21), IER) 
      IF (IER /= 0) IERPJ = 1 
      RETURN 
!----------------------- END OF SUBROUTINE DPREPJ ----------------------
      END                                           
