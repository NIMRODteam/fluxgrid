      SUBROUTINE XSETUN (LUN) 
      implicit none
!***BEGIN PROLOGUE  XSETUN                                              
!***PURPOSE  Reset the logical unit number for error messages.          
!***LIBRARY   MATHLIB                                                   
!***CATEGORY  R3B                                                       
!***TYPE      ALL (XSETUN-A)                                            
!***KEYWORDS  ERROR CONTROL                                             
!***DESCRIPTION                                                         
!                                                                       
!   XSETUN sets the logical unit number for error messages to LUN.      
!                                                                       
!***AUTHOR  Hindmarsh, Alan C., (LLNL)                                  
!***SEE ALSO  XERMSG, XERRWD, XERRWV                                    
!***REFERENCES  (NONE)                                                  
!***ROUTINES CALLED  IXSAV                                              
!***REVISION HISTORY  (YYMMDD)                                          
!   921118  DATE WRITTEN                                                
!   930329  Added SLATEC format prologue. (FNF)                         
!   930407  Corrected SEE ALSO section. (FNF)                           
!   930922  Made user-callable, and other cosmetic changes. (FNF)       
!***END PROLOGUE  XSETUN                                                
!                                                                       
! Subroutines called by XSETUN.. None                                   
! Function routine called by XSETUN.. IXSAV                             
!-----------------------------------------------------------------------
!**End                                                                  
      INTEGER LUN, JUNK, IXSAV 
!                                                                       
!***FIRST EXECUTABLE STATEMENT  XSETUN                                  
      IF (LUN > 0) JUNK = IXSAV (1,LUN,.TRUE.) 
      RETURN 
!----------------------- End of Subroutine XSETUN ----------------------
      END                                           
