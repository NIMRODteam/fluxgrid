      SUBROUTINE DPRJA (NEQ, Y, YH, NYH, EWT, FTEM, SAVF, WM, IWM,      &
     &   F, JAC)
      IMPLICIT NONE

      EXTERNAL F, JAC
      INTEGER NEQ, NYH, IWM
      REAL*8 Y, YH, EWT, FTEM, SAVF, WM
      DIMENSION NEQ(*), Y(*), YH(NYH,*), EWT(*), FTEM(*), SAVF(*),      &
     &   WM(*), IWM(*)
      INTEGER IOWND, IOWNS,                                             &
     &   ICF, IERPJ, IERSL, JCUR, JSTART, KFLAG, L,                     &
     &   LYH, LEWT, LACOR, LSAVF, LWM, LIWM, METH, MITER,               &
     &   MAXORD, MAXCOR, MSBP, MXNCF, N, NQ, NST, NFE, NJE, NQU
      INTEGER IOWND2, IOWNS2, JTYP, MUSED, MXORDN, MXORDS
      REAL*8 ROWNS,                                                     &
     &   CCMAX, EL0, H, HMIN, HMXI, HU, RC, TN, UROUND
      REAL*8 ROWND2, ROWNS2, PDNORM
      COMMON /DLS001/ ROWNS(209),                                       &
     &   CCMAX, EL0, H, HMIN, HMXI, HU, RC, TN, UROUND,                 &
     &   IOWND(6), IOWNS(6),                                            &
     &   ICF, IERPJ, IERSL, JCUR, JSTART, KFLAG, L,                     &
     &   LYH, LEWT, LACOR, LSAVF, LWM, LIWM, METH, MITER,               &
     &   MAXORD, MAXCOR, MSBP, MXNCF, N, NQ, NST, NFE, NJE, NQU
      !$omp threadprivate(/DLS001/)
      COMMON /DLSA01/ ROWND2, ROWNS2(20), PDNORM,                       &
     &   IOWND2(3), IOWNS2(2), JTYP, MUSED, MXORDN, MXORDS
      !$omp threadprivate(/DLSA01/)
      INTEGER I, I1, I2, IER, II, J, J1, JJ, LENP,                      &
     &   MBA, MBAND, MEB1, MEBAND, ML, ML3, MU, NP1
      REAL*8 CON, FAC, HL0, R, R0, SRUR, YI, YJ, YJJ,                   &
     &   DMNORM, DFNORM, DBNORM
!-----------------------------------------------------------------------
! DPRJA is called by DSTODA to compute and process the matrix
! P = I - H*EL(1)*J , where J is an approximation to the Jacobian.
! Here J is computed by the user-supplied routine JAC if
! MITER = 1 or 4 or by finite differencing if MITER = 2 or 5.
! J, scaled by -H*EL(1), is stored in WM.  Then the norm of J (the
! matrix norm consistent with the weighted max-norm on vectors given
! by DMNORM) is computed, and J is overwritten by P.  P is then
! subjected to LU decomposition in preparation for later solution
! of linear systems with P as coefficient matrix.  This is done
! by DGEFA if MITER = 1 or 2, and by DGBFA if MITER = 4 or 5.
!
! In addition to variables described previously, communication
! with DPRJA uses the following:
! Y     = array containing predicted values on entry.
! FTEM  = work array of length N (ACOR in DSTODA).
! SAVF  = array containing f evaluated at predicted y.
! WM    = real work space for matrices.  On output it contains the
!         LU decomposition of P.
!         Storage of matrix elements starts at WM(3).
!         WM also contains the following matrix-related data:
!         WM(1) = SQRT(UROUND), used in numerical Jacobian increments.
! IWM   = integer work space containing pivot information, starting at
!         IWM(21).   IWM also contains the band parameters
!         ML = IWM(1) and MU = IWM(2) if MITER is 4 or 5.
! EL0   = EL(1) (input).
! PDNORM= norm of Jacobian matrix. (Output).
! IERPJ = output error flag,  = 0 if no trouble, .gt. 0 if
!         P matrix found to be singular.
! JCUR  = output flag = 1 to indicate that the Jacobian matrix
!         (or approximation) is now current.
! This routine also uses the Common variables EL0, H, TN, UROUND,
! MITER, N, NFE, and NJE.
!-----------------------------------------------------------------------
      NJE = NJE + 1
      IERPJ = 0
      JCUR = 1
      HL0 = H*EL0
      GO TO (100, 200, 300, 400, 500), MITER
! If MITER = 1, call JAC and multiply by scalar. -----------------------
  100 LENP = N*N
      DO 110 I = 1,LENP
  110   WM(I+2) = 0.0
      CALL JAC (NEQ, TN, Y, 0, 0, WM(3), N)
      CON = -HL0
      DO 120 I = 1,LENP
  120   WM(I+2) = WM(I+2)*CON
      GO TO 240
! If MITER = 2, make N calls to F to approximate J. --------------------
  200 FAC = DMNORM (N, SAVF, EWT)
      R0 = 1000.0*ABS(H)*UROUND*N*FAC
      IF (R0 == 0.0) R0 = 1.0
      SRUR = WM(1)
      J1 = 2
      DO 230 J = 1,N
        YJ = Y(J)
        R = MAX(SRUR*ABS(YJ),R0/EWT(J))
        Y(J) = Y(J) + R
        FAC = -HL0/R
        CALL F (NEQ, TN, Y, FTEM)
        DO 220 I = 1,N
  220     WM(I+J1) = (FTEM(I) - SAVF(I))*FAC
        Y(J) = YJ
        J1 = J1 + N
  230   CONTINUE
      NFE = NFE + N
  240 CONTINUE
! Compute norm of Jacobian. --------------------------------------------
      PDNORM = DFNORM (N, WM(3), EWT)/ABS(HL0)
! Add identity matrix. -------------------------------------------------
      J = 3
      NP1 = N + 1
      DO 250 I = 1,N
        WM(J) = WM(J) + 1.0
  250   J = J + NP1
! Do LU decomposition on P. --------------------------------------------
      CALL DGEFA (WM(3), N, N, IWM(21), IER)
      IF (IER /= 0) IERPJ = 1
      RETURN
! Dummy block only, since MITER is never 3 in this routine. ------------
  300 RETURN
! If MITER = 4, call JAC and multiply by scalar. -----------------------
  400 ML = IWM(1)
      MU = IWM(2)
      ML3 = ML + 3
      MBAND = ML + MU + 1
      MEBAND = MBAND + ML
      LENP = MEBAND*N
      DO 410 I = 1,LENP
  410   WM(I+2) = 0.0
      CALL JAC (NEQ, TN, Y, ML, MU, WM(ML3), MEBAND)
      CON = -HL0
      DO 420 I = 1,LENP
  420   WM(I+2) = WM(I+2)*CON
      GO TO 570
! If MITER = 5, make MBAND calls to F to approximate J. ----------------
  500 ML = IWM(1)
      MU = IWM(2)
      MBAND = ML + MU + 1
      MBA = MIN(MBAND,N)
      MEBAND = MBAND + ML
      MEB1 = MEBAND - 1
      SRUR = WM(1)
      FAC = DMNORM (N, SAVF, EWT)
      R0 = 1000.0*ABS(H)*UROUND*N*FAC
      IF (R0 == 0.0) R0 = 1.0
      DO 560 J = 1,MBA
        DO 530 I = J,N,MBAND
          YI = Y(I)
          R = MAX(SRUR*ABS(YI),R0/EWT(I))
  530     Y(I) = Y(I) + R
        CALL F (NEQ, TN, Y, FTEM)
        DO 550 JJ = J,N,MBAND
          Y(JJ) = YH(JJ,1)
          YJJ = Y(JJ)
          R = MAX(SRUR*ABS(YJJ),R0/EWT(JJ))
          FAC = -HL0/R
          I1 = MAX(JJ-MU,1)
          I2 = MIN(JJ+ML,N)
          II = JJ*MEB1 - ML + 2
          DO 540 I = I1,I2
  540       WM(II+I) = (FTEM(I) - SAVF(I))*FAC
  550     CONTINUE
  560   CONTINUE
      NFE = NFE + MBA
  570 CONTINUE
! Compute norm of Jacobian. --------------------------------------------
      PDNORM = DBNORM (N, WM(ML+3), MEBAND, ML, MU, EWT)/ABS(HL0)
! Add identity matrix. -------------------------------------------------
      II = MBAND + 2
      DO 580 I = 1,N
        WM(II) = WM(II) + 1.0
  580   II = II + MEBAND
! Do LU decomposition of P. --------------------------------------------
      CALL DGBFA (WM(3), MEBAND, N, ML, MU, IWM(21), IER)
      IF (IER /= 0) IERPJ = 1
      RETURN
!----------------------- End of Subroutine DPRJA -----------------------
      END