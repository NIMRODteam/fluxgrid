      subroutine dscal(n,da,dx,incx) 
      implicit none
!                                                                       
!     scales a vector by a constant.                                    
!     uses unrolled loops for increment equal to one.                   
!     jack dongarra, linpack, 3/11/78.                                  
!                                                                       
      real*8 da,dx(1) 
      integer i,incx,m,mp1,n,nincx 
!                                                                       
      if(n<=0)return 
      if(incx==1)go to 20 
!                                                                       
!        code for increment not equal to 1                              
!                                                                       
      nincx = n*incx 
      do 10 i = 1,nincx,incx 
        dx(i) = da*dx(i) 
   10 continue 
      return 
!                                                                       
!        code for increment equal to 1                                  
!                                                                       
!                                                                       
!        clean-up loop                                                  
!                                                                       
   20 m = mod(n,5) 
      if( m == 0 ) go to 40 
      do 30 i = 1,m 
        dx(i) = da*dx(i) 
   30 continue 
      if( n < 5 ) return 
   40 mp1 = m + 1 
      do 50 i = mp1,n,5 
        dx(i) = da*dx(i) 
        dx(i + 1) = da*dx(i + 1) 
        dx(i + 2) = da*dx(i + 2) 
        dx(i + 3) = da*dx(i + 3) 
        dx(i + 4) = da*dx(i + 4) 
   50 continue 
      return 
      END                                           
