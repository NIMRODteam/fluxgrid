      SUBROUTINE DSOLSY (WM, IWM, X, TEM) 
      implicit none
!***BEGIN PROLOGUE  DSOLSY                                              
!***SUBSIDIARY                                                          
!***PURPOSE  ODEPACK linear system solver.                              
!***LIBRARY   MATHLIB (ODEPACK)                                         
!***TYPE      REAL*8 (SSOLSY-S, DSOLSY-D)                               
!***AUTHOR  Hindmarsh, Alan C., (LLNL)                                  
!***DESCRIPTION                                                         
!                                                                       
!  This routine manages the solution of the linear system arising from  
!  a chord iteration.  It is called if MITER .ne. 0.                    
!  If MITER is 1 or 2, it calls DGESL to accomplish this.               
!  If MITER = 3 it updates the coefficient h*EL0 in the diagonal        
!  matrix, and then computes the solution.                              
!  If MITER is 4 or 5, it calls DGBSL.                                  
!  Communication with DSOLSY uses the following variables:              
!  WM    = real work space containing the inverse diagonal matrix if    
!          MITER = 3 and the LU decomposition of the matrix otherwise.  
!          Storage of matrix elements starts at WM(3).                  
!          WM also contains the following matrix-related data:          
!          WM(1) = SQRT(UROUND) (not used here),                        
!          WM(2) = HL0, the previous value of h*EL0, used if MITER = 3. 
!  IWM   = integer work space containing pivot information, starting at 
!          IWM(21), if MITER is 1, 2, 4, or 5.  IWM also contains band  
!          parameters ML = IWM(1) and MU = IWM(2) if MITER is 4 or 5.   
!  X     = the right-hand side vector on input, and the solution vector 
!          on output, of length N.                                      
!  TEM   = vector of work space of length N, not used in this version.  
!  IERSL = output flag (in COMMON).  IERSL = 0 if no trouble occurred.  
!          IERSL = 1 if a singular matrix arose with MITER = 3.         
!  This routine also uses the COMMON variables EL0, H, MITER, and N.    
!                                                                       
!***SEE ALSO  LSODE                                                     
!***ROUTINES CALLED  DGBSL, DGESL                                       
!***COMMON BLOCKS    DLS001                                             
!***REVISION HISTORY  (YYMMDD)                                          
!   791129  DATE WRITTEN                                                
!   890501  Modified prologue to SLATEC/LDOC format.  (FNF)             
!   890503  Minor cosmetic changes.  (FNF)                              
!   930809  Renamed to allow single/real*8 versions. (ACH)              
!***END PROLOGUE  DSOLSY                                                
!**End                                                                  
      INTEGER IWM 
      INTEGER IOWND, IOWNS,                                             &
     &   ICF, IERPJ, IERSL, JCUR, JSTART, KFLAG, L, METH, MITER,        &
     &   MAXORD, MAXCOR, MSBP, MXNCF, N, NQ, NST, NFE, NJE, NQU         
      INTEGER I, MEBAND, ML, MU 
      REAL*8 WM, X, TEM 
      REAL*8 ROWNS,                                                     &
     &   CCMAX, EL0, H, HMIN, HMXI, HU, RC, TN, UROUND                  
      REAL*8 DI, HL0, PHL0, R 
      DIMENSION WM(*), IWM(*), X(*), TEM(*) 
      COMMON /DLS001/ ROWNS(209),                                       &
     &   CCMAX, EL0, H, HMIN, HMXI, HU, RC, TN, UROUND,                 &
     &   IOWND(12), IOWNS(6),                                           &
     &   ICF, IERPJ, IERSL, JCUR, JSTART, KFLAG, L, METH, MITER,        &
     &   MAXORD, MAXCOR, MSBP, MXNCF, N, NQ, NST, NFE, NJE, NQU         
      !$omp threadprivate(/DLS001/)
!                                                                       
!***FIRST EXECUTABLE STATEMENT  DSOLSY                                  
      IERSL = 0 
      GO TO (100, 100, 300, 400, 400), MITER 
  100 CALL DGESL (WM(3), N, N, IWM(21), X, 0) 
      RETURN 
!                                                                       
  300 PHL0 = WM(2) 
      HL0 = H*EL0 
      WM(2) = HL0 
      IF (HL0 == PHL0) GO TO 330 
      R = HL0/PHL0 
      DO 320 I = 1,N 
        DI = 1.0 - R*(1.0 - 1.0/WM(I+2)) 
        IF (ABS(DI) == 0.0) GO TO 390 
  320   WM(I+2) = 1.0/DI 
  330 DO 340 I = 1,N 
  340   X(I) = WM(I+2)*X(I) 
      RETURN 
  390 IERSL = 1 
      RETURN 
!                                                                       
  400 ML = IWM(1) 
      MU = IWM(2) 
      MEBAND = 2*ML + MU + 1 
      CALL DGBSL (WM(3), MEBAND, N, ML, MU, IWM(21), X, 0) 
      RETURN 
!----------------------- END OF SUBROUTINE DSOLSY ----------------------
      END                                           
