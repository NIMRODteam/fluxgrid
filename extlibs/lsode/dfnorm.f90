      REAL*8 FUNCTION DFNORM (N, A, W)
      IMPLICIT NONE
!-----------------------------------------------------------------------
! This function computes the norm of a full N by N matrix,
! stored in the array A, that is consistent with the weighted max-norm
! on vectors, with weights stored in the array W:
!   DFNORM = MAX(i=1,...,N) ( W(i) * Sum(j=1,...,N) ABS(a(i,j))/W(j) )
!-----------------------------------------------------------------------
      INTEGER N,   I, J
      REAL*8 A,   W, AN, SUM
      DIMENSION A(N,N), W(N)
      AN = 0.0
      DO 20 I = 1,N
        SUM = 0.0
        DO 10 J = 1,N
   10     SUM = SUM + ABS(A(I,J))/W(J)
        AN = MAX(AN,SUM*W(I))
   20   CONTINUE
      DFNORM = AN
      RETURN
!----------------------- End of Function DFNORM ------------------------
      END