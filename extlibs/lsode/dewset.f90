      SUBROUTINE DEWSET (N, ITOL, RTOL, ATOL, YCUR, EWT) 
      implicit none
!***BEGIN PROLOGUE  DEWSET                                              
!***SUBSIDIARY                                                          
!***PURPOSE  Set error weight vector.                                   
!***LIBRARY   MATHLIB (ODEPACK)                                         
!***TYPE      REAL*8 (SEWSET-S, DEWSET-D)                               
!***AUTHOR  Hindmarsh, Alan C., (LLNL)                                  
!***DESCRIPTION                                                         
!                                                                       
!  This subroutine sets the error weight vector EWT according to        
!      EWT(i) = RTOL(i)*ABS(YCUR(i)) + ATOL(i),  i = 1,...,N,           
!  with the subscript on RTOL and/or ATOL possibly replaced by 1 above, 
!  depending on the value of ITOL.                                      
!                                                                       
!***SEE ALSO  LSODE                                                     
!***ROUTINES CALLED  (NONE)                                             
!***REVISION HISTORY  (YYMMDD)                                          
!   791129  DATE WRITTEN                                                
!   890501  Modified prologue to SLATEC/LDOC format.  (FNF)             
!   890503  Minor cosmetic changes.  (FNF)                              
!   930809  Renamed to allow single/real*8 versions. (ACH)              
!***END PROLOGUE  DEWSET                                                
!**End                                                                  
      INTEGER N, ITOL 
      INTEGER I 
      REAL*8 RTOL, ATOL, YCUR, EWT 
      DIMENSION RTOL(*), ATOL(*), YCUR(N), EWT(N) 
!                                                                       
!***FIRST EXECUTABLE STATEMENT  DEWSET                                  
      GO TO (10, 20, 30, 40), ITOL 
   10 CONTINUE 
      DO 15 I = 1,N 
   15   EWT(I) = RTOL(1)*ABS(YCUR(I)) + ATOL(1) 
      RETURN 
   20 CONTINUE 
      DO 25 I = 1,N 
   25   EWT(I) = RTOL(1)*ABS(YCUR(I)) + ATOL(I) 
      RETURN 
   30 CONTINUE 
      DO 35 I = 1,N 
   35   EWT(I) = RTOL(I)*ABS(YCUR(I)) + ATOL(1) 
      RETURN 
   40 CONTINUE 
      DO 45 I = 1,N 
   45   EWT(I) = RTOL(I)*ABS(YCUR(I)) + ATOL(I) 
      RETURN 
!----------------------- END OF SUBROUTINE DEWSET ----------------------
      END                                           
