fcPsMap={}
###
##  Scalar quantities
#
fcPsMap["bToroidal"]=
fcPsMap["bTor"]=
fcPsMap["Bt"]=
# These two are the same and are the mean radius
fcPsMap["majorRadius"]=
fcPsMap["Rmajor"]=
fcPsMap["minorRadius"]=
fcPsMap["rminor"]=
fcPsMap["minorRadiusNormGLF"]=
fcPsMap["arho"]=
#
fcPsMap["ToroidalFlux"]=
fcPsMap["PoloidalFlux"]=
fcPsMap["shafranovShiftAxis"]=
fcPsMap["shafranovShiftEdge"]=
#
#
###
##  Profiles
#
fcPsMap["rho"]='RHO_EQ'
fcPsMap["rhoDim"]=
fcPsMap["NormPolFlux"]=
fcPsMap["rMinor"]=
fcPsMap["rmin"]=
fcPsMap["dRhodrMinor"]=
fcPsMap["DrDrho"]=
fcPsMap["rMajor"]=
fcPsMap["Rmaj"]=
fcPsMap["Routcore"]='R_MIDP_OUT'
fcPsMap["elongation"]='ELONG'
fcPsMap["kappa"]='ELONG'
fcPsMap["delta"]='TRIANG'
fcPsMap["deltaMiller"]=
fcPsMap["safetyFactor"]='Q_EQ'
fcPsMap["q"]='Q_EQ'
fcPsMap["gradSafetyFactor"]=
fcPsMap["gradRho"]='GRHO1'
fcPsMap["gradRhoSq"]='GRHO2'
fcPsMap["DlnQDlnRho"]=
fcPsMap["dKappaDRho"]=
fcPsMap["DkappaDrho"]=
fcPsMap["dDeltaDRho"]=
fcPsMap["DdeltaDrho"]=
fcPsMap["dDeltaMillerDRho"]=
fcPsMap["DdeltaMillerDrho"]=
fcPsMap["areaCrossSect"]='AREA'
fcPsMap["areaSurface"]='SURF'
fcPsMap["dRmajorDrho"]=
fcPsMap["DRmajDrho"]=
fcPsMap["nu_e"]=
fcPsMap["coulomb_log"]=
fcPsMap["RBtor"]=
fcPsMap["Routboard"]='R_MIDP_OUT'
fcPsMap["PsiNoutboard"]=
    Z_MIDP: flux surface midplane elevation
    R_SURFMAX: max R on flux surface
    Z_SURFMAX: max Z on flux surface
    R_SURFMIN: min R on flux surface
    Z_SURFMIN: min Z on flux surface
 
###
## Things to calculate
#
#fcPsMap["volumePrime"]= TAKE DERIVATIVE OF "VOL"
###
##  Initializaiton profiles
#
fcPsMap["rho"]=
fcPsMap["rhoDim"]=
fcPsMap["NormPolFlux"]=
fcPsMap["density_electron"]=
fcPsMap["temperature_electron"]=
fcPsMap["temperature_H2p1"]=
fcPsMap["pressure"]=
###
## Provenance
#
fcPsMap["SourceFile"]='EQDSK_FILE'
fcPsMap["SourceFileType"]='EQ_DATA_INFO'
# MATCH?
fcPsMap["SourceMapper"]='EQ_CODE_INFO'
#
